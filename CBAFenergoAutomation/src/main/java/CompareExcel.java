import java.io.*; 
import org.apache.poi.hssf.usermodel.HSSFWorkbook; 
import org.apache.poi.ss.usermodel.Cell; 
import org.apache.poi.ss.usermodel.Row; 
import org.apache.poi.ss.usermodel.Sheet; 
import org.apache.poi.ss.usermodel.Workbook; 

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;


public class CompareExcel {
	
	static StringBuilder sb=new StringBuilder();
	static StringBuilder sb1=new StringBuilder();
	static Workbook wb1 = new HSSFWorkbook(); 

	
    public static void main(String[] args) {
        try {
            // get input excel files
            FileInputStream excellFile1 = new FileInputStream(new File(
                    "C:\\Users\\sansingh\\Documents\\Excel comparison tool utility\\Before Merge.xlsx"));
            FileInputStream excellFile2 = new FileInputStream(new File(
                    "C:\\Users\\sansingh\\Documents\\Excel comparison tool utility\\Generation Tool - 8.7.3 DD.xlsx"));

            // Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook1 = new XSSFWorkbook(excellFile1);
            XSSFWorkbook workbook2 = new XSSFWorkbook(excellFile2);
            
            //

            // Get first/desired sheet from the workbook
            XSSFSheet sheet1 = workbook1.getSheetAt(0);
            XSSFSheet sheet2 = workbook2.getSheetAt(0);
         
           
          
            if(compareTwoSheets(sheet1, sheet2)) {
                System.out.println("\n\nThe two excel sheets are Equal");
            } else {
              System.out.println("\n\nThe two excel sheets are Not Equal");
            }
          
           //close files
            excellFile1.close();
            excellFile2.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    
    // Compare Two Sheets
    public static boolean compareTwoSheets(XSSFSheet sheet1, XSSFSheet sheet2) throws FileNotFoundException {
        int firstRow1 = sheet1.getFirstRowNum();;
        int lastRow1 =sheet1.getLastRowNum();
        System.out.println("last row is"+lastRow1);
        boolean equalSheets = true;
        for(int i=firstRow1; i <= lastRow1; i++) {
            
            System.out.println("\n\nComparing Row "+i);
            
            XSSFRow row1 = sheet1.getRow(i);
            XSSFRow row2 = sheet2.getRow(i);
            try {
				compareTwoRows(row1,row2);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//            if(!compareTwoRows(row1, row2)) {
//                equalSheets = false;
//                
//               
//                
//                System.out.println("Row "+i+" - Not Equal");
//                //break;
//            } else {
//                System.out.println("Row "+i+" - Equal");
//            }
        }
        return equalSheets;
    }

    // Compare Two Rows
    public static boolean compareTwoRows(XSSFRow row1, XSSFRow row2) throws IOException {
    	
    	  
         OutputStream os = new FileOutputStream("C:\\Users\\sansingh\\Documents\\Difference.xlsx"); 
         
        if((row1 == null) && (row2 == null)) {
            return true;
        } else if((row1 == null) || (row2 == null)) {
            return false;
        }
        
        int firstCell1 = 2;
        int lastCell1 = 6;
        boolean equalRows = true;
        Cell cell=null;
        Sheet  sheet=null;
        sheet =  wb1.createSheet("Difference1");
        
        // Compare all cells in a row
        for(int i=firstCell1; i <= lastCell1; i++) {
        	
        	
            XSSFCell cell1 = row1.getCell(i);
            XSSFCell cell2 = row2.getCell(i);
            String cellValueofsheet1 = cell1.getStringCellValue();
            String cellValueofsheet2=cell2.getStringCellValue();
            sb.append(cellValueofsheet1);
        	sb1.append(cellValueofsheet2);
        	Row row = sheet.createRow(i);
        	 cell = row.createCell(i);
        	sb.append(cell);
            
            if(!compareTwoCells(cell1, cell2)) {
                equalRows = false;
                
                System.err.println("       Cell "+i+" - NOt Equal");
                //break;
            } else {
                System.out.println("       Cell "+i+" - Equal");
            }
        }
       String Cellvalue=sb.toString();
        cell.setCellValue(Cellvalue); 
        wb1.write(os); 
        return equalRows;
        
    }

    // Compare Two Cells
    public static boolean compareTwoCells(XSSFCell cell1, XSSFCell cell2) {
    	StringBuilder sb=new StringBuilder();
    	StringBuilder sb1=new StringBuilder();  
        if((cell1 == null) && (cell2 == null)) {
            return true;
        } else if((cell1 == null)) {
        	System.out.println(cell2.getStringCellValue() + " is not present in parent sheet");
            return false;
        }
        else if((cell2 == null)) {
        	System.out.println(cell1.getStringCellValue() + " is not present in new sheet");
            return false;
        }
        boolean equalCells = false;
        int type1 = cell1.getCellType();
        int type2 = cell2.getCellType();
        if (type1 == type2) {
            if (cell1.getCellStyle().equals(cell2.getCellStyle())) {
                // Compare cells based on its type
                switch (cell1.getCellType()) {
                case HSSFCell.CELL_TYPE_FORMULA:
                    if (cell1.getCellFormula().equals(cell2.getCellFormula())) {
                        equalCells = true;
                    }
                    break;
                case HSSFCell.CELL_TYPE_NUMERIC:
                    if (cell1.getNumericCellValue() == cell2
                            .getNumericCellValue()) {
                        equalCells = true;
                    }
                    break;
                case HSSFCell.CELL_TYPE_STRING:
                    if (cell1.getStringCellValue().equals(cell2
                            .getStringCellValue())) {
                    	String cellValueofsheet1 = cell1.getStringCellValue();
                    	String cellValueofsheet2=cell2.getStringCellValue();
                    	sb.append(cellValueofsheet1);
                    	sb1.append(cellValueofsheet2);
                    	
                        equalCells = true;
                        
                        
                           
                    }
                    else {
                    	System.out.println(cell1.getStringCellValue() + " is not matching with "+cell2
                                .getStringCellValue());
                    }
                    
                    break;
                case HSSFCell.CELL_TYPE_BLANK:
                    if (cell2.getCellType() == HSSFCell.CELL_TYPE_BLANK) {
                        equalCells = true;
                    }
                    break;
                case HSSFCell.CELL_TYPE_BOOLEAN:
                    if (cell1.getBooleanCellValue() == cell2
                            .getBooleanCellValue()) {
                        equalCells = true;
                    }
                    break;
                case HSSFCell.CELL_TYPE_ERROR:
                    if (cell1.getErrorCellValue() == cell2.getErrorCellValue()) {
                        equalCells = true;
                    }
                    break;
                default:
                    if (cell1.getStringCellValue().equals(
                            cell2.getStringCellValue())) {
                        equalCells = true;
                    }
                    break;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
        return equalCells;
        
        
        
   
    }
    
    public static void compareTwoSheets1withoutSequence(XSSFSheet sheet1, XSSFSheet sheet2) {
    	for (int rowIndex = 1; rowIndex <= sheet1.getLastRowNum(); rowIndex++) {
    		XSSFRow row = sheet1.getRow(rowIndex);
    		//XSSFRow row1 = sheet1.getColumnBreaks();
    		
    		 if (row != null) {
    			 for (int i=1;i<=5;i++) {
    				 Cell cell = row.getCell(i);
    				 if (cell != null) {
    				 String cellValueofsheet1 = cell.getStringCellValue().trim();
    				 
    				 System.out.println("cell value is "+cellValueofsheet1);
    				 for(int rowIndex2 = 1; rowIndex2 <= sheet2.getLastRowNum(); rowIndex2++) {
    					 XSSFRow row2 = sheet2.getRow(rowIndex2);
    					 if (row2 != null) {
    						 for (int j=1;j<=5;j++) {
    							 
    							 Cell cell2 = row2.getCell(j);
    				 if (cell2 != null) {
    							 String cellValueofsheet2 = cell2.getStringCellValue().trim();
    							 if(cellValueofsheet1.equals(cellValueofsheet2)) {
		    						 System.out.println(cellValueofsheet1+"Both the sheets are equal"+cellValueofsheet2);
		    						 
		    						 
		    						 
		    							 
		    						 }
    							 else {
    								 System.out.println(cellValueofsheet1 +" is not eqaul to "+ cellValueofsheet2);
    								 
    							 }
    							 }
    						 }
    						 
    						 
    					 }
    					 
    					 
    				 }
    				 
    				 }
    			 }
    		 }
    		
    		
    		
    		
    		
    		
    		
    		
    	}
    
    	
    	}
    
    
}
    
        
    
    
//    for (int rowIndex2 = 1; rowIndex2 <= sheet2.getLastRowNum(); rowIndex2++) {
//    	XSSFRow row2 = sheet2.getRow(rowIndex);
//    	for (int j=1;j<=5;j++) {
//    	Cell cell2=row2.getCell(j);
//    	String cellValueoffirstsheet = cell.getStringCellValue();
//    	String cellValueofSecondsheet=cell2.getStringCellValue();
//    	
    
