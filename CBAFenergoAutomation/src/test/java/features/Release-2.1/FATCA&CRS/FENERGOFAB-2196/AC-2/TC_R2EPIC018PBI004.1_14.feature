#Test Case: TC_FENERGOFAB-2196_USTAX-AC02_COB
#PBI:FENERGOFAB-2196
#User Story ID: USTAX-005
#Designed by: Jagdev Singh
#Last Edited by: Jagdev Singh
Feature: TC_FENERGOFAB-2196_USTAX-AC02_COB

@COB
@Client - Corporate,BBG,FI,NBFI,PCG-Entity

Scenario: Verify US Onshore Obligations is not set Yes, when Booking Entity <> Bank - United States Branch 

Given I login to Fenergo Application with "RM:IBG-DNE" 
     When I complete "NewRequest" screen with key "Corporate" 
      And I complete "CaptureNewRequest" with Key "C1" and below data 
      | Product | Relationship | 
      | C1      | C1           | 
      When I navigate to add Product screen
      #Add Booking Entity <> Bank - United States Branch
      Then I add a product with Booking Entity <> Bank - USA Branch
      And I click on "Continue" button 
      When I complete "ReviewRequest" task 
      Then I store the "CaseId" from LE360 
  
     Given I login to Fenergo Application with "KYCMaker: Corporate" 
     When I search for the "CaseId"
     Then I store the "CaseId" from LE360  
     When I navigate to "ValidateKYCandRegulatoryGrid" task 
     When I complete "ValidateKYC" screen with key "C1" 
      And I click on "SaveandCompleteforValidateKYC" button 
  
     When I navigate to "EnrichKYCProfileGrid" task 
     When I add a "AnticipatedTransactionActivity" from "EnrichKYC" 
     When I complete "AddAddressFAB" task 
     Then I store the "CaseId" from LE360 
     When I complete "EnrichKYC" screen with key "C1" 
      And I click on "SaveandCompleteforEnrichKYC" button 
  
     When I navigate to "CaptureHierarchyDetailsGrid" task 
     When I add AssociatedParty by right clicking 
     When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
     When I complete "AssociationDetails" screen with key "Director" 
     When I complete "CaptureHierarchyDetails" task 
     
     When I navigate to "PreliminaryTaxAssessmentGrid" task 
     Then I select Do you have a US Tax Form from the client? as Yes
     Then I complete "PreliminaryTaxAssessmentGrid" task 
  
     When I navigate to "KYCDocumentRequirementsGrid" task 
     When I add a "DocumentUpload" in KYCDocument 
     Then I complete "KYCDocumentRequirements" task 
  
     When I navigate to "CompleteID&VGrid" task 
     When I complete "ID&V" task 
     When I complete "EditID&V" task 
     When I complete "AddressAddition" in "Edit Verification" screen 
     When I complete "Documents" in "Edit Verification" screen 
     When I complete "TaxIdentifier" in "Edit Verification" screen 
     When I complete "LE Details" in "Edit Verification" screen 
     When I click on "SaveandCompleteforEditVerification" button 
     When I complete "CompleteID&V" task 
  
     When I navigate to "CompleteRiskAssessmentGrid" task
      And I verify the populated risk rating is "Low"
     When I complete "RiskAssessment" task 
     
  	 #Verify US TAX classification is triggered
	 When I navigate to case details page 
     When I navigate to "USTaxClassification" task 
     #Verify US Onshore Obligations is not set Yes
	 Then I verify US Onshore Obligations is not set Yes
     Then I complete "USTaxClassification" task 
  
     Then I login to Fenergo Application with "RM:IBG-DNE" 
     When I search for the "CaseId"
     Then I store the "CaseId" from LE360 
     When I navigate to "ReviewSignOffGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "BUH:IBG-DNE" 
     When I search for the "CaseId" 
     When I navigate to "BHUReviewandSignOffGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "KYCMaker: Corporate" 
     When I search for the "CaseId" 
     Then I store the "CaseId" from LE360 
      And I complete "Waiting for UID from GLCMS" task from Actions button 
     When I navigate to "CaptureFabReferencesGrid" task 
     When I complete "CaptureFABReferences" task 
      And I assert that the CaseStatus is "Closed"
  