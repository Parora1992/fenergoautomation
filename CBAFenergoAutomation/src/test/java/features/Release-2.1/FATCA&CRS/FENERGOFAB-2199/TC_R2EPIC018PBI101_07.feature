#Test Case: TC_R2EPIC018PBI101_07
#PBI:R2EPIC018PBI101
#User Story ID: CRS 103
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2EPIC018PBI101_07 

Scenario:Validate if Complete CRS classification is triggered in Classification stage in COB workflow(CRS trigger rules should SET), then  user is not able to trigger Complete CRS classification in regular review workflow

	#Validate if Complete CRS classification is not triggered in Classification stage in COB workflow(CRS trigger rules should SET), then user is able to trigger Complete CRS classification in regular review workflow
	
	#PreCondition: Create entity with client type as PCG Entity and confidential as PCG Entity.
	Given I login to Fenergo Application with "RM:PCG Entity" 
	When I complete "NewRequest" screen with key "PCG Entity" 
	#Do not add product to LE
	And I complete "CaptureNewRequest"  
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	Given I login to Fenergo Application with "KYCMaker: PCG Entity" 
	When I search for the "CaseId" 
	Then I store the "CaseId" from LE360 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I select "Counter Party Type" as "Corporate" and "Legal Constitution Type" as "Special Purpose Entity"
	When I complete "EnrichKYC" screen with key "C1" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "AssociationDetails" screen with key "Director" 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "Preliminary Tax Assessment" task
	When I complete the "Preliminary Tax Assessment" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "ID&V" task 
	When I complete "EditID&V" task 
	When I complete "AddressAddition" in "Edit Verification" screen 
	When I complete "Documents" in "Edit Verification" screen 
	When I complete "TaxIdentifier" in "Edit Verification" screen 
	When I complete "LE Details" in "Edit Verification" screen 
	When I click on "SaveandCompleteforEditVerification" button 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task 
	When I complete "RiskAssessment" task 
	When I navigate to 'Classifications' stage
	#Validate Complete CRS classification is triggered in Classification stage
	Then I validate 'CRSClaasification' task is triggered
	When I complete "Classification" task    
    When I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Given I login to Fenergo Application with "KYCMaker: PCG Entity"
    When I search for the "CaseId"
    When I navigate to 'CaptureFABreferennces' task
    Then I complete 'CaptureFABreferennces' task
    And I validate case status is updated as 'closed'
			
	# Initiate RR workflow
    And I initiate "Regular Review" from action button
    And I initiate "Regular Review" from action button 
	When I complete "CloseAssociatedCase" task 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	Then I store the "CaseId" from LE360 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "ReviewRequestGrid" task 
	When I complete "ReviewRequest" task 
	When I navigate to "Review/EditClientDataTask" task
	When I complete "Review/EditClientData" Task
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "Preliminary Tax Assessment" task	
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	And I click on "ID&VLinkInRR" button 
	When I click on "SaveandCompleteforEditVerification" button 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task 
	When I complete "RiskAssessment" task
	#Validate Complete CRS classification is not triggered in Classification stage as it is triggered in COB workflow
   	When I navigate to "classification" stage
  	Then I validate "CRSclassification" is not triggered
  	When I complete "Classification" task    
  	
  	#Validate if Complete CRS classification is not triggered in Classification stage in COB workflow(CRS trigger rules should SET), then user is able to trigger Complete CRS classification in regular review workflow
  	#PreCondition: Create entity with client type as Corporate and confidential as Corporate.
	Given I login to Fenergo Application with "RM:IBG-DNE" 
	When I complete "NewRequest" screen with key "Corporate" 
	#Do not add product to LE
	And I complete "CaptureNewRequest"  
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId" 
	Then I store the "CaseId" from LE360 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I complete "EnrichKYC" screen with key "C1" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "AssociationDetails" screen with key "Director" 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "Preliminary Tax Assessment" task
	When I complete the "Preliminary Tax Assessment" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "ID&V" task 
	When I complete "EditID&V" task 
	When I complete "AddressAddition" in "Edit Verification" screen 
	When I complete "Documents" in "Edit Verification" screen 
	When I complete "TaxIdentifier" in "Edit Verification" screen 
	When I complete "LE Details" in "Edit Verification" screen 
	When I click on "SaveandCompleteforEditVerification" button 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task 
	When I complete "RiskAssessment" task 
	When I navigate to 'Classifications' stage
	#Validate Complete CRS classification is not triggered in Classification stage
	Then I validate 'CRSClaasification' task is not triggered
	When I complete "Classification" task    
    When I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to 'CaptureFABreferennces' task
    Then I complete 'CaptureFABreferennces' task
    And I validate case status is updated as 'closed'
			
	# Initiate RR workflow
    And I initiate "Regular Review" from action button
    And I initiate "Regular Review" from action button 
	When I complete "CloseAssociatedCase" task 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	Then I store the "CaseId" from LE360 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "ReviewRequestGrid" task 
	When I complete "ReviewRequest" task 
	When I navigate to "Review/EditClientDataTask" task 
	When I select "Counter Party Type" as "INSURANCE_COMPANY" and "Legal Constitution Type" as "Private Limited Company"
	When I complete "Review/EditClientData" Task
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "Preliminary Tax Assessment" task	
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	And I click on "ID&VLinkInRR" button 
	When I click on "SaveandCompleteforEditVerification" button 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task 
	When I complete "RiskAssessment" task
	#Validate Complete CRS classification is triggered in Classification stage as it is not triggered in COB workflow
   	When I navigate to "classification" stage
  	Then I validate "CRSclassification" is triggered
  	When I complete "Classification" task  
  	
  	
  	
  	
  	