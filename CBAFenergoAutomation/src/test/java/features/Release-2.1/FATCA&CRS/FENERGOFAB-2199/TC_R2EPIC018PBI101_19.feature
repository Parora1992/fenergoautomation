#Test Case: TC_R2EPIC018PBI101_19
#PBI:R2EPIC018PBI101
#User Story ID: CRS 107: 1-5, CRS 108, CRS 109
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2EPIC018PBI101_19 

	Scenario:Validate 'document requirement' generated in 'Document requirement' sub-flow is derived by the  value selected in 'Document Type' drop-down under CRS Detail sub-flow On Complete CRS classification task of COB workflow for Client type Corporate

	#Validate Notification 'Cannot save and complete as the document requirements have not been met" displays at the top of the 'Complete CRS classification' task screen if user does not fulfill the generated document requirement and user is not able to complete the task without fulfilling document requirement of COB workflow for Client type Corporate

	#Validate user is able to add comments in  'Comments' sub-flow and comments sub-flow display as mandatory on 'Add comments' screen of CRS Classification task of COB workflow

	#Validate user is able to Capture the 'Document Signed date' and 'Document received date' dates under CRS details sub-flow of Complete CRS calssification task of COB workflow for Client type Corporate

	#Validate when user navigates through Document sub-flow, then document category display as "AOF" and Doc type as "CRS & FATCA Self-Certification Form",  When Doc type "CRS & FATCA Self-Certification Form"  is selected on CRS details screen for COB Workflow for Client type Corporate

	#Validate when user navigates through Document sub-flow, then document category display as "MISC" and Document type "others ;When Doc type "CRS Waiver"  is selected on CRS details screen for COb workflow for Client type Corporate

	#Validate below Lovs for 'IF UAE resident, then visa is valid for 5 years or more' under 'Tax controlling Person details' sub-flow of Complete CRS Classifcation task screen in COB workflow for Client type Corporate
	#PreCondition: Create entity with client type as Corporate and confidential as IBG-DNE
	
	Given  I login to Fenergo Application with "RM:Corporate" 
	When  I complete "NewRequest" screen with key "Corporate" 
	#Do not add product to LE
	And  I complete "CaptureNewRequest" 
	And  I click on "Continue" button 
	When  I complete "ReviewRequest" task 
	Then  I store the "CaseId" from LE360 
	Given  I login to Fenergo Application with "KYCMaker:Corporate 
 	When  I search	for the "CaseId" 
	Then  I store the "CaseId" from LE360 
	When  I navigate to "ValidateKYCandRegulatoryGrid" task 
	When  I complete "ValidateKYC" screen with key "C1" 
	And  I click on "SaveandCompleteforValidateKYC" button 
	When  I navigate to "EnrichKYCProfileGrid" task 
	When I select "Counter Party Type" as "FINANCIAL_INSTITUTION_OTHER_THAN_BANK" and "Legal Constitution Type" as "Sovereign"
	When  I complete "EnrichKYC" screen with key "C1" 
	And  I click on "SaveandCompleteforEnrichKYC" button 
	When  I navigate to "CaptureHierarchyDetailsGrid" task 
	When  I complete "AssociationDetails" screen with key "Director" 
	When  I complete "CaptureHierarchyDetails" task 
	When  I navigate to "Preliminary Tax Assessment" task 
	When  I complete the "Preliminary Tax Assessment" task 
	When  I navigate to "KYCDocumentRequirementsGrid" task 
	Then  I complete "KYCDocumentRequirements" task 
	When  I navigate to "CompleteAMLGrid" task 
	Then  I complete "CompleteAML" task 
	When  I navigate to "CompleteID&VGrid" task 
	When  I complete "ID&V" task 
	When  I complete "EditID&V" task 
	When  I complete "AddressAddition" in "Edit Verification" screen 
	When  I complete "Documents" in "Edit Verification" screen 
	When  I complete "TaxIdentifier" in "Edit Verification" screen 
	When  I complete "LE Details" in "Edit Verification" screen 
	When  I click on "SaveandCompleteforEditVerification" button 
	When  I complete "CompleteID&V" task 
	When  I navigate to "CompleteRiskAssessmentGrid" task 
	When  I complete "RiskAssessment" task 
	When  I navigate to 'Classifications' stage 
	When  I navigate to 'CRSClaasification' task screen 
	Then  I validate 'CRSClaasification' task is not triggered 
	#Validate Complete CRS classification is triggered in Classification stage
	Then  I validate 'CRSClaasification' task is triggered 
	When  I select 'CRS & FATCA Self-Certification Form' from document type	drop-down 
	#validate the 'CRS & FATCA Self-Certification Form' is appearing as document requirement under Document requirement sub-flow
	Then  I validate the 'CRS & FATCA Self-Certification Form' is appearing as document requirement under Document requirement sub-flow 
	#Validate when user navigates through Document sub-flow, then document category display as "AOF" and Doc type as "CRS & FATCA Self-Certification Form",  When Doc type "CRS & FATCA Self-Certification Form"  is selected on CRS details screen
	When  I click on plus sign displaying at the top of document requirement sub-flow to add document 
	Then  I validate document category display as "AOF" and Doc type as	"CRS & FATCA Self-Certification Form" 
	
	When  I select 'CRS Waiver' from document type drop-down 
	#validate the 'CRS Waiver' is appearing as document requirement under Document requirement sub-flow
	Then  I validate the 'CRS Waiver' is appearing as document requirement under	Document requirement sub-flow 
	#Validate when user navigates through Document sub-flow, then document category display as "MISC" and Document type "others ;When Doc type "CRS Waiver"  is selected on CRS details screen
	When  I click on plus sign displaying at the top of document requirement sub-flow to add document 
	Then  I validate document category display as "MISC" and Doc type as "Others"on Add documents task screen 
	
	When  I click on 'Save and complete' button 
	#validate the notification Cannot save and complete as the document requirements have not been met" displays at the top of the 'Complete CRS classification' task screen
	Then  I validate the notification Cannot save and complete as the document requirements have not been met" displays at the top of the 'Complete CRS classification' task screen 
	When  I add document corresponding to document requirement 
	Then  I validate document is saved successfully 
	
	#Validate user is able to Capture the 'Document Signed date' and 'Document received date' dates under CRS details sub-flow and save dates successfully
	When  I add dates for 'Document Signed date' and 'Document received date' dates	under CRS details sub-flow 
	When  I click on save, dates are saved successfully 
	
	When  I navigate to Add comments screen by clicking on Plus button displaying at the top of comments sub-flow 
	#Validate user is able to add comments in  'Comments' sub-flow and comments sub-flow display as mandatory on 'Add comments' screen of CRS Classification task of COB workflow
	Then  I validate 'Comments' text box is displaying as mandatory 
	And  user is not able to proceed further without adding comments on	'Add comments' task screen 	When  I add comments and then click on save 
	Then  I verify comments are saved 
	
	When  I navigate back to 'CRSClaasification' task 
	When  I click on Plus sign displaying at the top of	'Tax controlling person details' 
	#Validate below Lovs for 'IF UAE resident, then visa is valid for 5 years or more' under 'Tax controlling Person details' sub-flow
	When  I click on 'IF UAE resident, then visa is valid for 5 years or more'	field 
	Then  I verify LOVs for	'IF UAE resident, then visa is valid for 5 years or more' field 
	|Yes, 5 years or more|
	|No, less than 5 years|
	|Not applicable|	
	And  I complete "Classification" task 
	
	
  	