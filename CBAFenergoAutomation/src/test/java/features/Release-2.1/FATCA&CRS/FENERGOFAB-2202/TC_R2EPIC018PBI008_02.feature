Feature: LEM - Regulatory Update

  #Test Case: TC_R2EPIC018PBI008_01
  #PBI: R2EPIC018PBI008
  #User Story ID: USTAX-011b
  #Designed by: Jagdev Singh
  #Last Edited by: Jagdev Singh
  @LEM
  Scenario: PCG Client - Validate only 2-Options(CRS and US tax) are avaialable in Select classification stage based on classications triggered in COB .
  
    #All the tasks in the RU are assigned to role group - Account Services.
    #Verify there is only one checker task in RU at stage-3 i.e.. Onboarding Review.
    #Verify there is no prelim tax classification triggered in any stage of RU.
    #RR is only available in LEM(LE Details/Area LOV), if we have CRS classification or US tax classifications are triggered in COB.
	#Verify only 2-Options(CRS and US tax) are avaialable in Select classification stage and these options too will based on classications triggered in COB.

	#######################################################################################
	Precondition: Create COB with Client Type = PCG Client, Confidential = PCG and Booking Country = Bank-UAE Branch with follwing Data.
	##On Enrich KYC profile sceen -> Counter Party Type is PARTNERSHIP and Legal Constitution Type Partnership
	##On prelim- tax assesment page -> Do you have a US Tax Form from the client? as Yes
    
    #######################################################################################
    Given I login to Fenergo Application with "RM:CBG"
    When I complete "NewRequest" screen with key "C1"
    And I complete "CaptureNewRequest" with Key "CBG" and below data
      | Product | Relationship |
      | C1      | C1           |
	#Add Booking Country as Bank - UAE Branch to trigger CRS classification
	Then I add Booking Country as Bank - UAE Branch
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: PCG Client"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
	#Select values to trigger US Tax and CRS classifications
	Then I select fields Counterparty Type = PARTNERSHIP and Legal Constitution Type = Partnership
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
	#Complete PreliminaryTaxAssessment task.
	When I navigate to PreliminaryTaxAssessment task
	Then I select Do you have a US Tax Form from the client? as Yes
	Then I complete "PreliminaryTaxAssessment" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
	
	#Verify US TAX classification is triggered.
    When I navigate to case details page
    Then I Verify US Tax classification is triggered.
	When I navigate to USTaxclassificationtask
    Then I complete US Tax classification task
	
	#Verify CRS TAX classification is triggered.
    When I navigate to case details page
    Then I Verify CRS Tax classification is triggered.
	When I navigate to CRSTaxclassificationtask
    Then I complete CRS Tax classification task
    
    Then I login to Fenergo Application with "RM:CBG"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "BUH:CBG"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "KYCMaker: PCG"
    When I search for the "CaseId"
    And I complete "Waiting for UID from GLCMS" task from Actions button
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    
    #LEM flow starts
    
    And I initiate "Maintenance Request" from action button
    When I select "LE Details" for field "Area" and select "Regulatory Updates" in field "LE Details Changes"
    And I click on "Submit" button
	
    Then I see "SelectClassifications" task is generated
	#Validate Prelimtaxclassification task is not triggered
	#Validate Select Classifications is assigned to "Account service Maker"
	The I validate task "SelectClassifications" is assigned to "Account service Maker" team
		And I navigate to "SelectClassifications" task
		
	#Validate only 2-classifications i.e. US Tax and CRS classifications are available to select in drop down field "Select Classifications to Trigger"
	    Then I validate only US Tax and CRS classifications are available drop down field "Select Classifications to Trigger"
		When I choose both US Tax and CRS classifications in drop down field "Select Classifications to Trigger"
		When I complete "SelectClassifications" task
		
	#Validate user new stage i.e. Classifications is available
	#Validate user navigates to next task i.e. CompleteUSTaxClassification and task is assigned to "Account service Maker"
	    Then I navigate to CompleteUSTaxClassification task
		Then I validate CompleteUSTaxClassification is assigned to "Account service Maker" team
		When I complete "CompleteUSTaxClassification" task
		
	#Validate user navigates to next task i.e. CompleteCRSClassification and task is assigned to "Account service Maker"
	    Then I navigate to CompleteCRSClassification task
		Then I validate CompleteCRSClassification is assigned to "Account service Maker" team
		When I complete "CompleteCRSClassification" task
		
	#Validate user new stage i.e. Onboarding Review is available	
	#Validate user navigates to next task i.e. Onboarding Review and task is assigned to "Account service Checker"
	    Then I navigate to OnboardingReview task
		Then I validate OnboardingReview is assigned to "Account service Checker" team
		Then I complete "OnboardingReview" task
	
	#I Validated Prelimtaxclassification task is not triggered in any stage of RU workflow
	#Regulatory Review case is closed.