#Test Case: TC_R2.1S2EPIC024PBI041.1_06
#PBI: TC_R2.1S2EPIC024PBI041.1_06
#User Story ID: Regular Review work flow for KYC LITE
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2.1S2EPIC024PBI041.1_06 

Scenario: #Validate If Regular review Workflow is not triggering  one day prior to Review start date for Lite KYC workflow for Client Type NBFI
	#Validate If Regular review Workflow is not triggering  one day prior to Review start date for Lite KYC workflow for Client Type FI

	Login and Creating an Onboarding flow for an FAB application
	Given I login to Fenergo Application with "RM:NBFI" 
	When I complete "NewRequest" screen with key "LiteKYC-NBFI" and legal entity Type as 'Brokers/Dealers (i.e. Securities Dealing Companies only)'
	And I complete "CaptureNewRequest" with Key "LiteKYC-NBFI" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	
	Given I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "LiteKYC-NBFI" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I complete "AddAddressFAB" task 
	
	When I complete "EnrichKYC" screen with key "LiteKYC-NBFI" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	When I complete "AssociationDetails" screen with key "Director" 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	Then I complete "KYCDocumentRequirements" task 
	
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
	Then I complete "CompleteAML" task 
	
	When I navigate to "CompleteID&VGrid" task 
	When I complete "ID&V" task 
	When I complete "EditID&V" task 
	When I complete "AddressAddition" in "Edit Verification" screen 
	When I complete "Documents" in "Edit Verification" screen 
	When I complete "TaxIdentifier" in "Edit Verification" screen 
	When I complete "LE Details" in "Edit Verification" screen 
	When I click on "SaveandCompleteforEditVerification" button 
	When I complete "CompleteID&V" task 
	
	When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessment" screen with key "LiteKYC" 
	
	Then I login to Fenergo Application with "RM:NBFI" 
	When I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "BUH:NBFI" 
	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	When I navigate to "CaptureFabReferencesGrid" task 
	When I complete "CaptureFABReferences" task 
	And I assert that the CaseStatus is "Closed" 
	
	#Validate If Regular review Workflow is not triggering  one day prior to Review start 
	#Run the query to change 'current date' as 'Review start date -1 day'
	#Verify 'current date' is displaying as 'Review start date-1 day'
	Then I validate 'current date' is one day less than 'review start date'
	#Verify Regular review case is not triggered automatically
	When I navigate to LE360-LEdetails screen 
	Then I validate 'Regular-review' case is not triggered automatically
	
	#Validate If Regular review Workflow is not triggering  one day prior to Review start date for Lite KYC workflow for Client Type FI
	Login and Creating an Onboarding flow for an FAB application
	Given I login to Fenergo Application with "RM:FI" 
	When I complete "NewRequest" screen with key "LiteKYC-FI" 
	And I complete "CaptureNewRequest" with Key "LiteKYC-FI" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	
	Given I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "LiteKYC-FI" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I complete "AddAddressFAB" task 
	
	When I complete "EnrichKYC" screen with key "LiteKYC-FI" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	When I complete "AssociationDetails" screen with key "Director" 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	Then I complete "KYCDocumentRequirements" task 
	
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
	Then I complete "CompleteAML" task 
	
	When I navigate to "CompleteID&VGrid" task 
	When I complete "ID&V" task 
	When I complete "EditID&V" task 
	When I complete "AddressAddition" in "Edit Verification" screen 
	When I complete "Documents" in "Edit Verification" screen 
	When I complete "TaxIdentifier" in "Edit Verification" screen 
	When I complete "LE Details" in "Edit Verification" screen 
	When I click on "SaveandCompleteforEditVerification" button 
	When I complete "CompleteID&V" task 
	
	When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessment" screen with key "LiteKYC" 
	
	Then I login to Fenergo Application with "RM:FI" 
	When I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "BUH:FI" 
	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	When I navigate to "CaptureFabReferencesGrid" task 
	When I complete "CaptureFABReferences" task 
	And I assert that the CaseStatus is "Closed" 
	
	#Validate If Regular review Workflow is not triggering  one day prior to Review start 
	#Run the query to change 'current date' as 'Review start date -1 day'
	#Verify 'current date' is displaying as 'Review start date-1 day'
	Then I validate 'current date' is one day less than 'review start date'
	#Verify Regular review case is not triggered automatically
	When I navigate to LE360-LEdetails screen 
	Then I validate 'Regular-review' case is not triggered automatically
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	