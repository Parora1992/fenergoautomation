#Test Case: TC_R2.1S2EPIC024PBI041.2_06
#PBI: TC_R2.1S2EPIC024PBI041.2_06
#User Story ID: Existing documents and client data in the system
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2.1S2EPIC024PBI041.2_06

Scenario: #Verify user is able to link already existing documents with the current document on Add document screen for Lite KYC workflow for Client type FI 

	Login and Creating an Onboarding flow for an FAB application
	Given I login to Fenergo Application with "RM:FI" 
	When I click on plus button to create new request 
	When I select Client Type "Financial Institution (FI)" 
	When I complete "NewRequest" screen with key "LiteKYC-FI" 
	And I complete "CaptureNewRequest" with Key "LiteKYC-FI" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	
	Given I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "LiteKYC-FI" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I complete "AddAddressFAB" task 
	
	When I complete "EnrichKYC" screen with key "LiteKYC-FI" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	When I complete "AssociationDetails" screen with key "Director" 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	Then I complete "KYCDocumentRequirements" task 
	
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
	Then I complete "CompleteAML" task 
	
	When I navigate to "CompleteID&VGrid" task 
	When I complete "ID&V" task 
	When I complete "EditID&V" task 
	When I complete "AddressAddition" in "Edit Verification" screen 
	When I complete "Documents" in "Edit Verification" screen 
	When I complete "TaxIdentifier" in "Edit Verification" screen 
	When I complete "LE Details" in "Edit Verification" screen 
	When I click on "SaveandCompleteforEditVerification" button 
	When I complete "CompleteID&V" task 
	
	When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessment" screen with key "LiteKYC" 
	
	Then I login to Fenergo Application with "RM:NBFI" 
	When I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "BUH:NBFI" 
	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	When I navigate to "CaptureFabReferencesGrid" task 
	When I complete "CaptureFABReferences" task 
	And I assert that the CaseStatus is "Closed" 
	
	#Verify the auto-Trigger of 'Regular review' case over KYC Lite workflow  when the case status is updated as closed
	#Run the query to change 'Review start date' as current date automatically since the Review start date is same as current date
	#Verify 'Review start date' is same as the 'current date'
	Then I validate 'Review start date' is same as the 'current date' 
	#Verify Regular review case is triggered 	
	When I navigate to LE360-LEdetails screen 
	Then I validate 'Regular-review' case is already triggered 
	
	#verify 'Close Associated Cases' task is displaying 'In Progress'
	And I validate 'Close Associated screen grid' task is displaying 'In Progress' 
	
	When I login to Fenergo Application with "KYCMaker:FIG" 
	When I navigate to  'Close Associated screen grid' task 
	When I click on 'Submit' button 
	Then I complete 'Close Associated screen grid' task 
	
	#verify 'Validate KYC and Regulatory Data' task is triggered
	When I navigate to  'Validate KYC and Regulatory Data grid' task 
	When I click on 'Save and complete' button 
	Then I complete 'Validate KYC and Regulatory Data grid' task 
	
	#verify 'Review Request Details' task is triggered
	When I navigate to  'Review Request Details' task 
	When I click on 'Save and complete' button 
	Then I complete 'Review Request Details' task 
	
	#verify 'Review/Edit Client Data' task is triggered
	When I navigate to  'Review/Edit Client Data' task 
	When I click on 'Save and complete' button 
	Then I complete 'Review/Edit Client Data' task 
	
	#verify 'KYC Document Requirement' task is triggered
	When I navigate to  'KYC Document Requirement' task 
	Then I Add the documents 
	When I click on 'Save and complete' button 
	Then I complete 'KYC Document Requirement' task 
	
	#verify 'KYC Document Requirement' task is triggered
	When I navigate to  'KYC Document Requirement' task 
	When I click on 'options' button and 'Attach document'
	When I select 'existing' from 'document source' drop-down
	#Add the Document ID that is already Present in the case
	When I add 'Document ID' of already exeisting document and click on 'Search' button
	Then I see a document is displaying under 'Advanced search' sub-flow
	When I select the document and click on 'Save' button
	#validate Document is added corresponding to doc requirement
	Then I validate document is added corresponding to the document requirement 
	And the status is updated as 'received'
	Then I complete 'KYC Document Requirement' task by adding all documents
	