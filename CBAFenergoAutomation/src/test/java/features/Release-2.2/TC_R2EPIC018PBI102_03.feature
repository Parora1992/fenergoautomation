Feature: LEM-RU - US Tax/CRS: New feature to link existing Documents on "Documents Panel" in Classification screen.

  #Test Case: TC_R2EPIC018PBI102_03
  #PBI: R2EPIC018PBI102
  #AC ID: AC-1
  #Designed by: Jagdev Singh
  #Last Edited by: Jagdev Singh
  #LEM-RU
  client type-PCG-Entity
    Scenario  : Valdiate the "CRS & FATCA Self-Certification Form" Document added by RM in COB or in RR or added in document page(from LE360 page)should be visible in CRS Classification and US tax classification Suggested document grid as suggested document which user can "Link" in LEM-RU workflow
    #Validate : The "CRS & FATCA Self-Certification Form" uploaded by user in COB and RR is available in LEM-RU
	#Validate : User can upload a new document i.e. "CRS & FATCA Self-Certification Form" in document page on LE360 page.
	#Validate : The document uploaded by the user previously is available/visible in CRS Classification and US tax classification as "suggested" in which user can "Link".
	#validate : The "status" in the Suggested document grid is "Pending" when we generate "CRS & FATCA Self-Certification Form" document on CRS Classification and US tax classification document grid
	#Validate : User can view the document uploaded/attached previously for "CRS & FATCA Self-Certification Form" in Suggested document grid
    #Validate : User can "Link" the same(uploaded previously)document in on CRS Classification and US tax classification against "CRS & FATCA Self-Certification Form" in  Suggested document grid
	#valdiate : The "status" of the document "CRS & FATCA Self-Certification Form" is changed to "Recieved"/Complete once we "Link" the document to previouly added document
	#Validate : User can add a new document for CRS & FATCA Self-Certification if user don't want to Link the previously attached document
	#valdiate : Validate the new added document for CRS & FATCA Self-Certification will replace the "previously attached document" and will be Linked automatically in suggested document grid
	#validate : User will not able to move to next stage unless user don't link CRS & FATCA Self-Certification with suggested document or don't attach a new one.
	#Validate : "View Document" document option is not available in "CRS Classification and US tax classification" when a fileless document wa attached previously
	
 ########################################################################################################################
   #PreCondition: Already created New LE with client type as PCG-Entity and confidential as PCG
   #PreCondition: Add "CRS & FATCA Self-Certification Form" document on "Document" page on LE-360 page
  #####################################################################################################################  

     Given I login to Fenergo Application with "RM:IBG-DNE" 
     When I complete "NewRequest" screen with key "PCG-Entity" 
      And I complete "CaptureNewRequest" with Key "C1" and below data 
      | Product | Relationship | 
      | C1      | C1           | 
      And I click on "Continue" button 
     When I complete "ReviewRequest" task 
     Then I store the "CaseId" from LE360 
  
    Given I login to Fenergo Application with "KYCMaker: PCG" 
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360  
    When I navigate to "ValidateKYCandRegulatoryGrid" task 
    When I complete "ValidateKYC" screen with key "C1" 
    And I click on "SaveandCompleteforValidateKYC" button 
	
	#Add a new document from LE360 page->"Documents" page
	When I search for the "CaseId"
    Then I store the "CaseId" from LE360  
	When I navigate to LE360 page->"Documents" page and Click on + button
	I add a new "CRS & FATCA Self-Certification Form" document and attach a document to it
	    
     When I navigate to "EnrichKYCProfileGrid" task 
     When I add a "AnticipatedTransactionActivity" from "EnrichKYC" 
	 #Select Counterparty Type and Legal Constitution Type as "Partnership" to trigger US tax and CRS tax classifications
	 Then I select fields Counterparty Type = PARTNERSHIP and Legal Constitution Type = Partnership
     When I complete "AddAddressFAB" task 
     Then I store the "CaseId" from LE360 
     When I complete "EnrichKYC" screen with key "C1" 
      And I click on "SaveandCompleteforEnrichKYC" button 
  
     When I navigate to "CaptureHierarchyDetailsGrid" task 
     When I add AssociatedParty by right clicking 
     When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
     When I complete "AssociationDetails" screen with key "Director" 
     When I complete "CaptureHierarchyDetails" task 
	 
	 When I navigate to "PreliminaryTaxAssessmentGrid" task 
     Then I complete "PreliminaryTaxAssessmentGrid" task 
  
     When I navigate to "KYCDocumentRequirementsGrid" task 
     When I add a "DocumentUpload" in KYCDocument 
     Then I complete "KYCDocumentRequirements" task 
  
     When I navigate to "CompleteID&VGrid" task 
     When I complete "ID&V" task 
     When I complete "EditID&V" task 
     When I complete "AddressAddition" in "Edit Verification" screen 
     When I complete "Documents" in "Edit Verification" screen 
     When I complete "TaxIdentifier" in "Edit Verification" screen 
     When I complete "LE Details" in "Edit Verification" screen 
     When I click on "SaveandCompleteforEditVerification" button 
     When I complete "CompleteID&V" task 
  
     When I navigate to "CompleteRiskAssessmentGrid" task
      And I verify the populated risk rating is "Low"
     When I complete "RiskAssessment" task 
	 
	 #Verify US TAX classification is triggered.
     When I navigate to case details page
     Then I Verify US Tax classification is triggered
	 When select "Tax Form Type" as "CRS & FATCA Self-Certification Form" and click on "Save and update button"
	 #Document "CRS & FATCA Self-Certification Form" will visible under Suggested Documents grid
	 Then I validate "CRS & FATCA Self-Certification Form" is visible under Suggested Documents grid
	 #valdiate The "status" of the document is "Pending" in Suggested Documents grid
	 Then I validate The "status" of the document is "Pending"
	 #Validate User can view the document uploaded/attached previously for "CRS & FATCA Self-Certification Form"
	 Then I click on "Actions" context menu againt "CRS & FATCA Self-Certification Form" document and click on "View Document"
	 Then I valdiate the same document we attached previously is available
	 #Validate "View Document" document option is not available in "CRS Classification and US tax classification" if fileless document was uploaded in previous stages
	 When I click on "Actions" context menu againt "CRS & FATCA Self-Certification Form" document "fileless" document (was uploaded in previous stages)
	 Then I validate "View Document" is not available for a Fileless document
	 #Validate User will not able to move to next stage unless user don't "Link" CRS & FATCA Self-Certification with suggested document or don't add a "New document"
	 When I click on "Save and Complete" button without "Link" CRS & FATCA Self-Certification with suggested document or not add a "New document" for the same
	 Then I valdiate warning message on top of the scree "Cannot save and complete as all the document requirements have not been met"
	 #Validate user can "Link" the same(uploaded previously)document against "CRS & FATCA Self-Certification Form" in  Suggested Documents grid
	 When I click on "Actions" context menu againt "CRS & FATCA Self-Certification Form" document in Suggested Documents grid
	 Then I valdiate "Link" option is available to link "CRS & FATCA Self-Certification Form" with previously added document
     When I click on "Link"
	 #Valdiate "CRS & FATCA Self-Certification Form" is linked with previouly added document
	 Then I valdiate the "CRS & FATCA Self-Certification Form" is linked with previouly added document
	 #valdiate The "status" of the document "CRS & FATCA Self-Certification Form" is changed to "Recieved" in Suggested Documents grid once we "Link" the document to previouly added document
	 Then I validate The "status" of the document is changed to "Recieved"
	 #Validate User can add a new "CRS & FATCA Self-Certification form" document if user don't wish to "Link" the previously attached document
	 Then I upload a new document against "CRS & FATCA Self-Certification form" document
	 #Verify the new uploaded document is already "Linked"
	 Then I verify already uploaded document is already "Linked" 
     Then I complete US Tax classification task	
	
	 #Verify CRS TAX classification is triggered.
	 When I navigate to case details page
     Then I Verify CRS TAX classification is triggered
	 When select "Tax Form Type" as "CRS & FATCA Self-Certification Form" and click on "Save and update button"
	 #Document "CRS & FATCA Self-Certification Form" will visible under Suggested Documents grid
	 Then I validate "CRS & FATCA Self-Certification Form" is visible under Suggested Documents grid
	 #valdiate The "status" of the document is "Pending" in Suggested Documents grid
	 Then I validate The "status" of the document is "Pending"
	 #Validate User can view the document uploaded/attached previously for "CRS & FATCA Self-Certification Form"
	 Then I click on "Actions" context menu againt "CRS & FATCA Self-Certification Form" document and click on "View Document"
	 Then I valdiate the same document we attached previously is available
	 #Validate "View Document" document option is not available in "CRS Classification and US tax classification" if fileless document was uploaded in previous stages
	 When I click on "Actions" context menu againt "CRS & FATCA Self-Certification Form" document "fileless" document (was uploaded in previous stages)
	 Then I validate "View Document" is not available for a Fileless document
	 #Validate User will not able to move to next stage unless user don't "Link" CRS & FATCA Self-Certification with suggested document or don't add a "New document"
	 When I click on "Save and Complete" button without "Link" CRS & FATCA Self-Certification with suggested document or not add a "New document" for the same
	 Then I valdiate warning message on top of the scree "Cannot save and complete as all the document requirements have not been met"
	 #Validate user can "Link" the same(uploaded previously)document against "CRS & FATCA Self-Certification Form" in  Suggested Documents grid
	 When I click on "Actions" context menu againt "CRS & FATCA Self-Certification Form" document in Suggested Documents grid
	 Then I valdiate "Link" option is available to link "CRS & FATCA Self-Certification Form" with previously added document
     When I click on "Link"
	 #Valdiate "CRS & FATCA Self-Certification Form" is linked with previouly added document
	 Then I valdiate the "CRS & FATCA Self-Certification Form" is linked with previouly added document
	 #valdiate The "status" of the document "CRS & FATCA Self-Certification Form" is changed to "Recieved" in Suggested Documents grid once we "Link" the document to previouly added document
	 Then I validate The "status" of the document is changed to "Recieved"
	 #Validate User can add a new "CRS & FATCA Self-Certification form" document if user don't wish to "Link" the previously attached document
	 Then I upload a new document against "CRS & FATCA Self-Certification form" document
	 #Verify the new uploaded document is already "Linked"
	 Then I verify already uploaded document is already "Linked" 
	 Then I complete CRS Tax classification task	
  
     Then I login to Fenergo Application with "RM:IBG-DNE" 
     When I search for the "CaseId"
     Then I store the "CaseId" from LE360 
     When I navigate to "ReviewSignOffGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "BUH:IBG-DNE" 
     When I search for the "CaseId" 
     When I navigate to "BHUReviewandSignOffGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "KYCMaker: PCG-Entity" 
     When I search for the "CaseId" 
     Then I store the "CaseId" from LE360 
     And I complete "Waiting for UID from GLCMS" task from Actions button 
     When I navigate to "CaptureFabReferencesGrid" task 
     When I complete "CaptureFABReferences" task 
     And I assert that the CaseStatus is "Closed"
	  
	#Comment:=LEM - Regulatory Update
	And I initiate "Maintenance Request" from action button
    When I select "LE Details" for field "Area" and select "Regulatory Updates" in field "LE Details Changes"
    And I click on "Submit" button
	
    Then I see "SelectClassifications" task is generated
	And I navigate to "SelectClassifications" task
	When I choose US Tax classification and CRS Tax classification in drop down field "Select Classifications to Trigger"
	When I complete "SelectClassifications" task
		
	#Verify US TAX classification is triggered.
     When I navigate to case details page
     Then I Verify US Tax classification is triggered
	 When select "Tax Form Type" as "CRS & FATCA Self-Certification Form" and click on "Save and update button"
	 #Document "CRS & FATCA Self-Certification Form" will visible under Suggested Documents grid
	 Then I validate "CRS & FATCA Self-Certification Form" is visible under Suggested Documents grid
	 #valdiate The "status" of the document is "Pending" in Suggested Documents grid
	 Then I validate The "status" of the document is "Pending"
	 #Validate User can view the document uploaded/attached previously for "CRS & FATCA Self-Certification Form"
	 Then I click on "Actions" context menu againt "CRS & FATCA Self-Certification Form" document and click on "View Document"
	 Then I valdiate the same document we attached previously is available
	 #Validate "View Document" document option is not available in "CRS Classification and US tax classification" if fileless document was uploaded in previous stages
	 When I click on "Actions" context menu againt "CRS & FATCA Self-Certification Form" document "fileless" document (was uploaded in previous stages)
	 Then I validate "View Document" is not available for a Fileless document
	 #Validate User will not able to move to next stage unless user don't "Link" CRS & FATCA Self-Certification with suggested document or don't add a "New document"
	 When I click on "Save and Complete" button without "Link" CRS & FATCA Self-Certification with suggested document or not add a "New document" for the same
	 Then I valdiate warning message on top of the scree "Cannot save and complete as all the document requirements have not been met"
	 #Validate user can "Link" the same(uploaded previously)document against "CRS & FATCA Self-Certification Form" in  Suggested Documents grid
	 When I click on "Actions" context menu againt "CRS & FATCA Self-Certification Form" document in Suggested Documents grid
	 Then I valdiate "Link" option is available to link "CRS & FATCA Self-Certification Form" with previously added document
     When I click on "Link"
	 #Valdiate "CRS & FATCA Self-Certification Form" is linked with previouly added document
	 Then I valdiate the "CRS & FATCA Self-Certification Form" is linked with previouly added document
	 #valdiate The "status" of the document "CRS & FATCA Self-Certification Form" is changed to "Recieved" in Suggested Documents grid once we "Link" the document to previouly added document
	 Then I validate The "status" of the document is changed to "Recieved"
	 #Validate User can add a new "CRS & FATCA Self-Certification form" document from "Document" grid if user don't wish to "Link" the previously attached document
	 When I click on + button document to add a new "CRS & FATCA Self-Certification form" 
	 Then I add a add a new document "CRS & FATCA Self-Certification form"
	 #Valdidate the new upload document via "Documents" grid is available in "Suggested Documents" grid to "Link"
	 Then I validate the new upload document via "Documents" grid will replace old (previouly uploaded document)document and is available in "Suggested Documents" grid to "Link" 
     Then I complete US Tax classification task	
	
	 #Verify CRS TAX classification is triggered.
	 When I navigate to case details page
     Then I Verify CRS TAX classification is triggered
	 When select "Tax Form Type" as "CRS & FATCA Self-Certification Form" and click on "Save and update button"
	 #Document "CRS & FATCA Self-Certification Form" will visible under Suggested Documents grid
	 Then I validate "CRS & FATCA Self-Certification Form" is visible under Suggested Documents grid
	 #valdiate The "status" of the document is "Pending" in Suggested Documents grid
	 Then I validate The "status" of the document is "Pending"
	 #Validate User can view the document uploaded/attached previously for "CRS & FATCA Self-Certification Form"
	 Then I click on "Actions" context menu againt "CRS & FATCA Self-Certification Form" document and click on "View Document"
	 Then I valdiate the same document we attached previously is available
	 #Validate "View Document" document option is not available in "CRS Classification and US tax classification" if fileless document was uploaded in previous stages
	 When I click on "Actions" context menu againt "CRS & FATCA Self-Certification Form" document "fileless" document (was uploaded in previous stages)
	 Then I validate "View Document" is not available for a Fileless document
	 #Validate User will not able to move to next stage unless user don't "Link" CRS & FATCA Self-Certification with suggested document or don't add a "New document"
	 When I click on "Save and Complete" button without "Link" CRS & FATCA Self-Certification with suggested document or not add a "New document" for the same
	 Then I valdiate warning message on top of the scree "Cannot save and complete as all the document requirements have not been met"
	 #Validate user can "Link" the same(uploaded previously)document against "CRS & FATCA Self-Certification Form" in  Suggested Documents grid
	 When I click on "Actions" context menu againt "CRS & FATCA Self-Certification Form" document in Suggested Documents grid
	 Then I valdiate "Link" option is available to link "CRS & FATCA Self-Certification Form" with previously added document
     When I click on "Link"
	 #Valdiate "CRS & FATCA Self-Certification Form" is linked with previouly added document
	 Then I valdiate the "CRS & FATCA Self-Certification Form" is linked with previouly added document
	 #valdiate The "status" of the document "CRS & FATCA Self-Certification Form" is changed to "Recieved" in Suggested Documents grid once we "Link" the document to previouly added document
	 Then I validate The "status" of the document is changed to "Recieved"
	 #Validate User can add a new "CRS & FATCA Self-Certification form" document from "Document" grid if user don't wish to "Link" the previously attached document
	 When I click on + button document to add a new "CRS & FATCA Self-Certification form" 
	 Then I add a add a new document "CRS & FATCA Self-Certification form"
	 #Valdidate the new upload document via "Documents" grid is available in "Suggested Documents" grid to "Link"
	 Then I validate the new upload document via "Documents" grid will replace old (previouly uploaded document)document and is available in "Suggested Documents" grid to "Link" 
	 Then I complete CRS Tax classification task	
	
	#Regulatory Review case is closed.	
    