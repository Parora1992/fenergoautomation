#Test Case: TC_R1S3EPIC007PBI002_04
#PBI: R1S3EPIC007PBI002
#User Story ID: D1.9
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Internal Documents

  Scenario: Validate "Onboarding Maker" is able to upload Internal Documents like(Internal Approvals, queries, etc., emails, etc) on Document details screen of "Enrich KYC profile" stage.
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "OnBoardingMaker"
    When I search for "Caseid"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "EnrichKYCProfileGrid" task as "OnboardingManager"
    When I complete to "EnrichKYCProfileGrid" task as "OnboardingManager"
    When I navigate to "KYCDocumentrequirement" task.
    When I click on "AttachDocument" from options button displaying corresponding to document requirement
    When I navigate to "DocumentDetails" screen
    #Test-data: validate user is able to add (Internal Approvals, queries, emails, etc)
    When I add an email
    Then I can see email is appearing as the uploaded document
    When I click on 'Save' button
    Then I can see email is added as uploaded document and details are appearing under document requirement
