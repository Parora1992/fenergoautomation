#Test Case: TC_R1S5EPICDM009_02
#PBI: R1S5EPICDM008
#User Story ID: NA
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: DM Screen 2 - Fircosoft screening 

 
  Scenario: Validate if DM user is able to add Fircosoft Screening screen to Migrated Entity and associated party 
  Given I login to Fenergo Application with "DM user"
  And I navigate to DM Screen-2 #using url
  And I provide value for "T24 CIF ID" field 
  And I click on "SEARCH" button
  And I assert the migrated legal entity is fetched and shown in the result grid
 	And I click on button "Add Selected"
 	And I am redirected to "Capture Hierarchy Details" screen
 	And I assert the migrated entity is visible in the hierarchy
 	And I click "Add Associated Party" link
 	And I am redirected to "Associated Parties" screen
 	And I provide data for mandatory fields
  And I click on "CREATE NEW LE" button
  And I am redirected to "Association Details" screen of added associated party
  And I fill data for mandatory fields
  And I click on "SAVE" button
  And I assert that the associated party is added in the hierarchy successfully 	
 
 	#Add Fircosoft Screening by clikcing the actions(...) button from tree view for migrated entity and click "Add Fircosoft Screening"
	And I add "FircosoftScreening" for the entity 
	#To perform below step, scroll down the screen and click "Edit" from the Actions (...) of the LE in the "Assessments" section
	And I navigate to "Assessment" screen 
	#To perform below step, click "Edit" from the Actions (...) in the "Active Screenings" section
	And I navigate to "FircosoftScreening" screen 
	And I choose postive match for PEP and Sanctions
	And I save and complete the screening
	#Please refer the mock up screen in the PBI
	And I assert value 1 is reflected PEP and Sanctions under "Confirmed Matches" section
	
	#Add Fircosoft Screening by clikcing the actions(...) button from tree view  for associated party and click "Add Fircosoft Screening"
	And I add "FircosoftScreening" for the associated party 
	#To perform below step, scroll down the screen and click "Edit" from the Actions (...) of the LE in the "Assessments" section
	And I navigate to "Assessment" screen 
	#To perform below step, click "Edit" from the Actions (...) in the "Active Screenings" section
	And I navigate to "FircosoftScreening" screen 
	And I save and complete the screening
	
	
	
	