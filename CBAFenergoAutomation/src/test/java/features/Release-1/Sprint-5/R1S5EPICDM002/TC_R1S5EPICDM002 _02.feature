#Test Case: TC_R1S5EPICDM002_02
#PBI: R1S5EPICDM002
#User Story ID: US091, US101, US029, US025, US087, US026, US039, US033, US090, US027,
#US038, US037, US086, US034, US035, US030, US041, US088, US093, US028, US099, US092, US089, US089, US024, US040, US139, US002, US003
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1S5EPICDM002_02

  @Automation
  Scenario: Corporate:Validate the field behaviour in Customer Details section of DM Screen-1 for Corporate Client type
    Given I login to Fenergo Application with "RM"
    #Create DM Request with Client type = Corporate and Country of Incorporation / Establishment = UAE
    #Click on + button and then click on New DM Request
    When I navigate to DM Request screen
    #Validate field behaviours in Customer Details section
    And I validate the following fields in "Customer Details" Sub Flow
      | Label                                                      | FieldType    | Visible | Editable | Mandatory | Field Defaults To |
      | FAB Segment                                                | Drop-down    | true    | true     | true      |                   |
      | Country of Domicile/Physical Presence                      | Drop-down    | true    | true     | true      | Select...         |
      | Date of Incorporation                                      | Date         | true    | true     | true      |                   |
      | Purpose of Account/Relationship                            | Alphanumeric | true    | true     | false     |                   |
      | Residential Status                                         | Drop-down    | true    | true     | true      | Autopopulated     |
      | Justification for opening/maintaining non-resident account |              | false   |          |           |                   |
      | Relationship with bank                                     | Drop-down    | true    | true     | true      | Select...         |
      | CIF Creation Required?                                     | Check box    | true    | true     | true      | true              |
      | Group Name                                                 | Alphanumeric | true    | true     | false     |                   |
      | Trading/Operation Name                                     | Alphanumeric | true    | true     | true      |                   |
      | Legal Counter party type                                   | Drop-down    | true    | true     | true      | Select...         |
      | Legal Constitution Type                                    | Drop-down    | true    | true     | true      | Select...         |
      | Customer Relationship Status                               | Drop-down    | true    | true     | false     | Select...         |
      | Real Name                                                  | Alphanumeric | true    | true     | false     |                   |
      | Original Name                                              | Alphanumeric | true    | true     | false     |                   |
      | Legal Entity Name (Parent)                                 | Alphanumeric | true    | true     | false     |                   |
      | Emirate                                                    | Drop-down    | true    | true     | true      | Select...         |
      | Length of Relationship                                     | Drop-down    | true    | true     | true      | Select...         |
      | Entity Level                                               |              | false   |          |           |                   |
      | Registration Number                                        | Alphanumeric | true    | true     | true      |                   |
      | Name of Registration Body                                  | Alphanumeric | true    | true     | true      |                   |
      | SWIFT Address                                              |              | false   |          |           |                   |
      | Does the entity have a previous name(s)?                   | Drop-down    | true    | true     | true      | Select...         |
      | Previous Name(s)                                           |              | false   |          |           |                   |
      | Website Address                                            | Alphanumeric | true    | true     | false     |                   |
      | Channel & Interface                                        | Drop-down    | true    | true     | true      | Select...         |
      | Customer Tier                                              | Drop-down    | true    | true     | true      | Select...         |
      | Is UAE Licensed                                            | Drop-down    | true    | true     | false     | True              |
      | KYC Approval Date                                          | Date         | true    | true     | false     |                   |
      | Compliance Review Date                                     | Date         | true    | true     | false     |                   |
    #Verify Date of Incorporation field does not accepts future date.
    And I fill the data for "DMScreen-1" with key "Data4"
      #Verify the Residential Status is autopopulated to 'Resident' if the Country of Incorporation / Establishment = UAE
      | Label                                                      | FieldType    | Visible | Editable | Mandatory | Field Defaults To |
      | Residential Status                                         | Drop-down    | true    | true     | true      | Resident          |
      #Verify the Residential Status is autopopulated to 'Non Resident' if the Country of Incorporation / Establishment Not equal to UAE
      | Label                                                      | FieldType    | Visible | Editable | Mandatory | Field Defaults To |
      | Residential Status                                         | Drop-down    | true    | true     | true      | Non Resident      |
      #Verify 'Justification for opening/maintaining non-resident account' is visible and mandatory if Residental Status = Non Resisident (Country of Incorporation / Establishment Not equal to UAE)
      | Label                                                      | FieldType    | Visible | Editable | Mandatory | Field Defaults To |
      | Justification for opening/maintaining non-resident account | Alphanumeric | true    | true     | true      |                   |
    #Verify Trading/Operation Name field NOT accepts special characters. Test Data:Special characters
    And I fill the data for "DMScreen-1" with key "Data1"
    #Verify Trading/Operation Name field NOT accepts lower case characters. Test Data: Lower case char
    And I fill the data for "DMScreen-1" with key "Data2"
    #Verify Trading/Operation Name field accepts valid characters other than special and lowercase.
    And I fill the data for "DMScreen-1" with key "Data3"
    #Verify Legal Entity Name (parent) field NOT accepts special characters. Test Data:Special characters
    And I fill the data for "DMScreen-1" with key "Data1"
    #Verify Legal Entity Name (parent) field NOT accepts lower case characters. Test Data: Lower case char
    And I fill the data for "DMScreen-1" with key "Data2"
    #Verify Legal Entity Name (parent) field accepts valid characters other than special and lowercase.
    And I fill the data for "DMScreen-1" with key "Data3"
      #Verify 'Emirate' field is NOT visible if the Country of Incorporation / Establishment not equal to UAE
      | Label            | FieldType    | Visible | Editable | Mandatory | Field Defaults To |
      | Emirate          |              | false   |          |           |                   |
      #Verify 'Previous Name(s) field is Visible and Mandatory if the 'Does the entity have a previous name(s)?' = Yes
      | Label            | FieldType    | Visible | Editable | Mandatory | Field Defaults To |
      | Previous Name(s) | Alphanumeric | true    | true     | true      |                   |
    #Verify Website field does not accept duplicate value
    And I fill the data for "DMScreen-1" with key "Data4"
      #Verify 'Is UAE Licensed' field is defaulted to False if Country of Incorporation / Establishment Not equal to UAE
      | Label           | FieldType | Visible | Editable | Mandatory | Field Defaults To |
      | Is UAE Licensed | Dropdown  | true    | true     | false     | False             |
    #Validate the FAB Segments lovs (FABsegmentcode&desc-Refer to LOV tab in the PBI R1EPIC002PBI005)
    And I validate the LOV of "FABsegmentcode" with key "fabseglov"
    #Validate the Country of Domicile/Physical Presence and country of incorporation lovs (Countries-Refer to LOV tab in the PBI R1EPIC002PBI005)
    And I validate the LOV of "CountryofDomicile" with key "countrieslov"
    #Validate the Residential Status lovs
    Then I validate the specific LOVs for "Residential Status"
      | Lovs         |
      | Resident     |
      | Non Resident |
    #Validate the Relationship with bank lovs
    Then I validate the specific LOVs for "Relationship with bank"
      | Lovs          |
      | Borrowing     |
      | Non Borrowing |
    #Validate the Legal Counter party type lovs (Legal Counterparty Type-Refer to LOV tab in the PBI R1EPIC002PBI005)
    And I validate the LOV of "LegalCounterpartyType" with key "legcoupartylov"
    #Validate the Legal Constitution Type lovs (Legal Constitution Type-Refer to LOV tab in the PBI R1EPIC002PBI005)
    #Dynamic list displayed based on Legal Counterparty type selected
    And I validate the LOV of "LegalConstitutionType" with key "legconstypelov"
    #Validate the Customer Relationship Status lovs
    Then I validate the specific LOVs for "Customer Relationship Status"
      | Lovs         |
      | Active       |
      | Closed       |
      | Inactive     |
      | Black Listed |
    #Validate the Emirates lovs when Country of Incorporation / Establishment = UAE
    Then I validate the specific LOVs for "Emirates"
      | Lovs           |
      | Abu Dhabi      |
      | Ajman          |
      | Dubai          |
      | Fujairah       |
      | Ras Al Khaimah |
      | Sharjah        |
      | Umm Al Quwain  |
    #Validate the 'Length of Relationship' lovs
    Then I validate the specific LOVs for "Length of Relationship"
      | Lovs             |
      | New Customer     |
      | Over > 12 Months |
      | Less < 12 Months |
    #Validate the 'Entity Level' lovs
    Then I validate the specific LOVs for "Entity Level"
      | Lovs                               |
      | Affiliates                         |
      | Branches                           |
      | Head offices                       |
      | SPV                                |
      | Subsidiaries/Representative office |
    #Validate the 'Does the entity have a previous name(s)?' lovs
    Then I validate the specific LOVs for "Does the entity have a previous name(s)?"
      | Lovs |
      | Yes  |
      | No   |
    #Verify the lovs for "Channel & Interface" field
    Then I validate the specific LOVs for "Channel & Interface"
      | Lovs                                                                        |
      | Over the counter/Face-to-face (direct banking, branch/relationship manager) |
      | Non-face-to-face (agents, representative, third parties)                    |
      | Others                                                                      |
    #Verify the lovs for "Customer Tier" field
    Then I validate the specific LOVs for "Customer Tier"
      | Lovs     |
      | Platinum |
      | GOLD     |
      | Silver   |
      | Bronze   |
    #Validate the 'Is UAE Licensed' lovs
    Then I validate the specific LOVs for "Is UAE Licensed"
      | Lovs  |
      | True  |
      | False |
		#Verify the ability to enter / select all mandatory and non-mandatory values in Customer Details section