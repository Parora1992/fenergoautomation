#Test Case: TC_R1S5EPICDM002_08
#PBI: R1S5EPICDM002
#User Story ID: US129, US131, US132, US133, US138, US134, US135, US136, US137, US082,
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1S5EPICDM002_08

  @Automation
  Scenario: NBFI:Validate the field behaviour in KYC Conditions and Regulatory Data sections of DM Screen-1 for NBFI Client type
    Given I login to Fenergo Application with "RM"
    #Create DM Request with Client type = NBFI and Country of Incorporation / Establishment = UAE
    #Click on + button and then click on New DM Request
    When I navigate to DM Request screen
    #Validate field behaviours in KYC Condition section
    And I validate the following fields in "KYC condition" Sub Flow
      | Label                                                                      | FieldType | Visible | Editable | Mandatory | Field Defaults To |
      | Is this entity publicly listed?                                            | Drop-down | true    | true     | true      | Select...         |
      | Name of Stock Exchange                                                     |           | false   |          |           |                   |
      | Stock Exchange Domicile Country                                            |           | false   |          |           |                   |
      | Is this FAB's Recognized Stock Exchange?                                   |           | false   |          |           |                   |
      | Is this parent entity publicly listed?                                     |           | false   |          |           |                   |
      | Is this entity regulated?                                                  | Drop-down | true    | true     | false     | Autopopulated     |
      | Name of Regulatory Body                                                    |           | false   |          |           |                   |
      | Is this a Prohibited client (as per FAB�s AML/CTF/Sanctions Policy)?       | Drop-down | true    | true     | true      | Select...         |
      | Specify the prohibited Category                                            |           | false   |          |           |                   |
      | Has the exceptional approvals been obtained to on-board/retain the client? |           | false   |          |           |                   |
      | Types of Shares (Bearer/Registered)                                        | Drop-down | true    | true     | true      | Select...         |
      | Is the Entity operating with Flexi Desk?                                   | Drop-down | true    | true     | true      | Select...         |
    #Verify below field are Visible and Mandatory if the field 'Is this entity publicly listed?' = Yes
    And I validate the following fields in "KYC condition" Sub Flow
      | Label                                    | FieldType             | Visible | Editable | Mandatory | Field Defaults To |
      | Name of Stock Exchange                   | Multiselect drop-down | true    | true     | true      | Select...         |
      | Stock Exchange Domicile Country          | Multiselect drop-down | true    | true     | true      | Select...         |
      | Is this FAB's Recognized Stock Exchange? | Multiselect drop-down | true    | true     | true      | Select...         |
    #Verify 'Is this parent entity publicly listed? is Visible and Mandatory if the field 'Is this entity publicly listed?' = No
    And I validate the following fields in "KYC condition" Sub Flow
      | Label                                  | FieldType             | Visible | Editable | Mandatory | Field Defaults To |
      | Is this parent entity publicly listed? | Multiselect drop-down | true    | true     | true      | Select...         |
    #Verify below field are Visible and Mandatory if the field 'Is this entity publicly listed?' = No and 'Is this parent entity publicly listed?= Yes
    And I validate the following fields in "KYC condition" Sub Flow
      | Label                                    | FieldType             | Visible | Editable | Mandatory | Field Defaults To |
      | Name of Stock Exchange                   | Multiselect drop-down | true    | true     | true      | Select...         |
      | Stock Exchange Domicile Country          | Multiselect drop-down | true    | true     | true      | Select...         |
      | Is this FAB's Recognized Stock Exchange? | Multiselect drop-down | true    | true     | true      | Select...         |
    #Verify 'Name of Regulatory Body' is Visible and Mandatory if the field 'Is this entity regulated?' = Yes
    And I validate the following fields in "KYC condition" Sub Flow
      | Label                   | FieldType | Visible | Editable | Mandatory | Field Defaults To |
      | Name of Regulatory Body | Drop-down | true    | true     | true      | Select...         |
    #Verify below field are Visible and Mandatory if the field 'Is this a Prohibited client (as per FAB�s AML/CTF/Sanctions Policy)?' = Yes
    And I validate the following fields in "KYC condition" Sub Flow
      | Label                                                                      | FieldType    | Visible | Editable | Mandatory | Field Defaults To |
      | Specify the prohibited Category                                            | Alphanumeric | true    | true     | true      | Select...         |
      | Has the exceptional approvals been obtained to on-board/retain the client? | Drop-down    | true    | true     | true      | Select...         |
    #Validate the Save button is disabled when 'Has the exceptional approvals been obtained to on-board/retain the client? = NO
    And I check that below data is available
      | FieldLabel | Enabled |
      | SAVE       | No      |
    #Validate the 'Is this entity publicly listed?' lovs
    Then I validate the specific LOVs for "Is this entity publicly listed?"
      | Lovs |
      | Yes  |
      | No   |
    #Validate the 'Is this FAB's Recognized Stock Exchange?' lovs
    Then I validate the specific LOVs for "Is this FAB's Recognized Stock Exchange?"
      | Lovs |
      | Yes  |
      | No   |
    #Validate the 'Is this entity regulated?' lovs
    Then I validate the specific LOVs for "Is this entity regulated?"
      | Lovs |
      | Yes  |
      | No   |
    #Validate the 'Is this a Prohibited client (as per FAB�s AML/CTF/Sanctions Policy)?' lovs
    Then I validate the specific LOVs for "Is this a Prohibited client (as per FAB�s AML/CTF/Sanctions Policy)?"
      | Lovs |
      | Yes  |
      | No   |
    #Validate the 'Is the Entity operating with Flexi Desk?' lovs
    Then I validate the specific LOVs for "Is the Entity operating with Flexi Desk?"
      | Lovs |
      | Yes  |
      | No   |
    #Validate the 'Is this parent entity publicly listed?' lovs
    Then I validate the specific LOVs for "Is the Entity operating with Flexi Desk?"
      | Lovs |
      | Yes  |
      | No   |
    #Validate the 'Types of Shares (Bearer/Registered)' lovs
    Then I validate the specific LOVs for "Types of Shares (Bearer/Registered)"
      | Lovs                                               |
      | Ownership Structures Include Issuing Bearer Shares |
      | Registered Shares                                  |
    #Validate the 'Has the exceptional approvals been obtained to on-board/retain the client?' lovs
    Then I validate the specific LOVs for "Has the exceptional approvals been obtained to on-board/retain the client?"
      | Lovs |
      | Yes  |
      | No   |
    #Validate the Name of Regulatory Body lovs (LookupRegulator-Regulator list-Refer to LOV tab in the PBI R1EPIC002PBI005)
    And I validate the LOV of "LookupRegulator-Regulator list" with key "reglov"
    #Validate the Name of Stock Exchange lovs (Stock Exchange-Refer to LOV tab in the PBI R1EPIC002PBI006)
    And I validate the LOV of "Name of Stock Exchange" with key "stocklov"
    #Validate the Stock Exchange Domicile Country lovs (Countries-Refer to LOV tab in the PBI R1EPIC002PBI005)
    And I validate the LOV of "CountryofDomicile" with key "countrieslov"
    #Regulatory Data Section
    #Validate field behaviours in Regulatory Data section
    And I validate the following fields in "Regulatory Data" Sub Flow
      | Label                                              | FieldType             | Visible | Editable | Mandatory | Field Defaults To |
      | Countries of Business Operations/Economic Activity | MultiSelect Drop-down | true    | true     | true      | Select...         |
		#Verify the ability to enter / select all mandatory and non-mandatory values in KYC Conditions and Regulatory Data sections