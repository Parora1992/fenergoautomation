#Test Case: TC_R1S5EPICDM012_04
#PBI: R1S5EPICDM012
#User Story ID:
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1S5EPICDM012_04

    Scenario: Verify the ability to add more subflows for Unsuccessful Entities which is created without adding all the subflows
    Given I login to Fenergo Application with "DMuser"
    #Create DM Request with Client type = Corporate/PCG-Entity/FI/NBFI/BBG and Country of Incorporation / Establishment = UAE or any other country
    #Click on + button and then click on New DM Request
    When I navigate to DM Request screen
    #Fill in all the mandatory and non-mandatory fields and click on Save button
    #DO NOT Add all the subflows 
    #Searching existing DM Entity
    #Click on + button and then click on New DM Request and select 'Existing' in DM Request type
    #Enter T24 CIF ID (for the entiy created without adding all the sub flows)
    #Validate the entity is available in search result grid and all the sub flows are editable
    #Add all the sub flows
    #Verify all the sub flows added without any errors
