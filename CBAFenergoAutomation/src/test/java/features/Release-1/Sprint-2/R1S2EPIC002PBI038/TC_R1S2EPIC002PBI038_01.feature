#Test Case: TC_R1S2EPIC002PBI038_01
#PBI: R1S2EPIC002PBI038
#User Story ID: US068
#Designed by: Niyaz Ahmed (as part of R1EPIC002PBI008)
#Last Edited by: Vibhav Kumar
Feature: TC_R1S2EPIC002PBI038_01-Capture New Request Details - Complete Legal Entity Category

  @Automation @TC_R1S2EPIC002PBI038_01
  Scenario: 
    Validate the 'Legal Entity Category' dropdown is filtered with relevant values (13) in Complete screen and other secondary screens when 'Client Type' is selected as 'Corporate' from the Enter entity details screen (Refer lov in the PBI)

    Given I login to Fenergo Application with "RM:IBG-DNE"
    #Select client Type as 'Corporate' in Enter entity details screen
    #When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    #Navigate to Complete screen (3 screen in the workflow)
    When I navigate to "LegalEntityCategoryWithoutSubmit" button with ClientType as "Corporate"
    Then I check that below data is not visible
    |FieldLabel						|
    |Legal Entity Category|
    #=Regression Comments on 04-04-21:Legal Entity Category is now no longer present in the screen hence commented 
    #Then I verify "Legal Entity Category" drop-down values with ClientType as "Corporate"
    When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    
    #When I complete "NewRequest" screen with key "C1"
    #Refer PBI for LOVs
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    #=Regression Comments on 04-04-21:Legal Entity Category is now no longer present in the screen hence commented 
    #Then I verify "Legal Entity Category" drop-down values with ClientType as "Corporate"
    #Refer PBI for LOVs
    Then I check that below data is not visible
    |FieldLabel						|
    |Legal Entity Category|
    And I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
	#    And I check that "LegalEntityCategory" is readonly
	#    And I Validate the Legal entity category value is defaulted from complete screen/Validate KYC and Regulatory Data
