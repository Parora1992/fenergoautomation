#Test Case: TC_R1EPIC011PBI006_05
#PBI: R1EPIC011PBI006
#User Story ID: US094 (PRIMARY), US095 (PRIMARY), US096 (PRIMARY), US097 (PRIMARY)
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1EPIC011PBI006_05

  @To_be_automated
  Scenario: Validate behaviour of below fields in Industry Codes Details section of Enrich KYC Profile screen
    #Primary Industry of Operation
    #Secondary Industry of Operation
    #Primary Industry of Operation Islamic
    #Primary Industry of Operation UAE
    Given I login to Fenergo Application with "RM"
    #Creating a legal entity with "Client Entity Type" as "Business Banking Group" and "Legal Entity Role" as "Client/Counterparty" and "Country of Incorporation" as "AE-UNITED ARAB EMIRATES"
    When I create new request with ClientEntityType as "BusinessBankingGroup" and LegalEntityrole as "Client/Counterparty" and CountryOfIncorporation as "AE-UNITED ARAB EMIRATES"
    And I navigate to "CaptureRequestDetailsFAB" task
    #Test data C10: Select "Head Office_ISB-100" value for "UID Orginating Branch" field and fill all mandatory data
    And I complete "CaptureRequestDetailsFAB" task with key "C10"
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    #Verify if only the following fields are visible under "Industry Codes Details" section in the mentioned order
    And I validate the only following fields in "Industry Codes Details" section
      | Fenergo Label Name                    | Field Type | Visible | Editable | Mandatory   | Field Defaults To |
      | Primary Industry of Operation         | Drop-down  | Yes     | Yes      | Yes         | Select...         |
      | Secondary Industry of Operation       | Drop-down  | Yes     | Yes      | No          | Select...         |
      | Primary Industry of Operation Islamic | Drop-down  | Yes     | No       | Conditional | blank             |
      | Primary Industry of Operation UAE     | Drop-down  | Yes     | Yes      | Yes         | blank             |
    And I validate "Central Index Key" field is not visible
    And I validate "Stock Exchange Code" field is not visible
    And I validate "NACE 2 Code" field is not visible
    And I validate "ISIN" field is not visible
    And I validate "NAIC" field is not visible
    And I validate "SWIFT BIC" field is not visible
    And I validate LOVs of "Primary Industry of Operation" field
    #Refer PBI for LOV list (only dummy list is available in PBI as of 30 Aug)
    And I select "Dummy 1" for "Primary Industry of Operation" field
    And I validate LOVs of "Secondary Industry of Operation" field
    #Refer PBI for LOV list (only dummy list is available in PBI as of 30 Aug and this should be same as LOVs of "Primary Industry of Operation" as per PBI)
    And I validate "Secondary Industry of Operation" field is multiselect drop-down
    #Validate "Primary Industry of Operation Islamic" field is defauled to with the value chosen for "Primary Industry of Operation" field
    And I validate "Primary Industry of Operation Islamic" field is defaulted to "Dummy 1"
    And I validate "Primary Industry of Operation Islamic" field is not-editable
    And I validate LOVs of "Primary Industry of Operation UAE" field
    #Refer PBI for LOV list (only dummy list is available in PBI as of 30 Aug and this should be same as LOVs of "Primary Industry of Operation" as per PBI)
    And I validate "Primary Industry of Operation UAE" field is defaulted to "Dummy 1"
    And I validate "Primary Industry of Operation UAE" field is editable
    When I complete "EnrichKYCProfileFAB" task
    And I refer back case to "New Request" stage
    #Edit Country of Incorporation to "AF-AFGHANISTAN"
    And I complete "CaptureRequestDetailsFAB" task with CountryOfIncorporation as "AF-AFGHANISTAN"
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    And I validate "Primary Industry of Operation Islamic" field is defaulted to blank
    And I validate "Primary Industry of Operation Islamic" field is not-editable
    And I select "Dummy 2" for "Primary Industry of Operation" field
    And I validate "Primary Industry of Operation UAE" field is defaulted to "Dummy 2"
    And I validate "Primary Industry of Operation UAE" field is editable
    When I complete "EnrichKYCProfileFAB" task
