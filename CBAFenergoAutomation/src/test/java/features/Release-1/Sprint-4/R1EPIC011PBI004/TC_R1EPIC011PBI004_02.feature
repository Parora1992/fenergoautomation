#Test Case: TC_R1EPIC011PBI004_02
#PBI: R1EPIC011PBI004
#User Story ID: US026
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1EPIC011PBI004_02 

@Automation
Scenario:
Verify 'Justification for opening/maintaining non-resident account' field is NOT available when Residential status = resident 
#Create entity with Country of Incorporation = UAE and client type = Business Banking Group
	Given I login to Fenergo Application with "RM:BBG" 
	#Select client Type as 'Business Banking group' in Enter entity details screen
	When I complete "NewRequest" screen with key "BBG" 
	#Navigate to Capture request details screen
	And I check that below data is not visible 
		| FieldLabel                                                       | 
		| Justification for opening/maintaining non-resident account 			 | 
