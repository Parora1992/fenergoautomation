#Test Case: TC_R1S3EPIC007PBI001.2_01
#PBI: R1S3EPIC007PBI001.2
#User Story ID: D1.4
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Document Deletion

  Scenario: Validate "Onboardingmaker" is able to unlink added document corresponding to document requirement from document grid on document on "KYC Document requirement" task
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "OnBoardingMaker"
    When I search for "Caseid"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "EnrichKYCProfileGrid" task as "OnboardingManager"
    When I complete to "EnrichKYCProfileGrid" task as "OnboardingManager"
    When I navigate to "KYCDocumentrequirement" task.
    When I add a document corresponding to document requirement
    Then I expand the document requirement and can see document is displaying under document reuirement
    #Test-data: Validate user is able to unlink attached document
    When I click on "unlink" button
    Then I can see attached document is removed under document requirement

  Scenario: Validate "Onboardingmaker" is able to remove added document corresponding to document requirement from document grid under "documents" section
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureRequestDetailsFAB" task
    When I navigate to "Documents" section
    When I add a document by clicking on Plus sign
    Then I can see document is added under "Documents" section
    #Test-data: Validate user is able to remove attached document
    When I click on "Remove" button
    Then I can see attached document is removed under document requirement
