#Test Case: TC_R1S3EPIC007PBI001.1_02
#PBI: R1S3EPIC007PBI001.1
#User Story ID: D1.1
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Upload Document (DMS)

  Scenario: Validate Upload service gets triggered (content and structure of XML) when RM user uploads a document on Document details screen of Capture request details task of 'New request stage'
  #Placeholder for DMS Testing 