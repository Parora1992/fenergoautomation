#Test Case: TC_R1EPIC002PBI302_05
#PBI: R1EPIC002PBI302
#User Story ID:
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1EPIC002PBI302_05

  @TobeAutomated
  Scenario: Clienttype-NBFI:Validate the field behaviours in Customer Details section and GLCMS section of Capture Request Details and Review request screen
    #Validate the field behaviours in KYC Conditions section of Validate KYC and Regulatory screen
    Given I login to Fenergo Application with "RM:NBFI"
    #Create entity with client type = NBFI
    When I complete "NewRequest" screen with key "Non FI"
    When I navigate to "CaptureNewRequest" task
    #Validate the below fields ae hidden in customer details section
    And I validate the following fields in "Customer Details" Sub Flow
      | Label                                                           | Visible |
      | Is this entity publicly listed?                                 | false   |
      | Name of Stock Exchange                                          | false   |
      | Stock Exchange Domiciled in FAB's Recognised List of Countries? | false   |
      | Country of Stock Exchange                                       | false   |
      | Is this entity regulated?                                       | false   |
      | Regulated By                                                    | false   |
      | Regulatory ID                                                   | false   |
      | FAB Segment                                                     | false   |
      | Group name                                                      | false   |
      | SWIFT BIC                                                       | false   |
    #Validate GLCMS section is hidden in Review request screen
    And I validate the following fields in "Customer Details" Sub Flow
      | Label | Visible |
      | GLCMS | false   |
    #Verify the Legal entity Name field value is autopopulated the value from Enter entity details screen
    #Verify Legal Entity Name field NOT accepts special characters. Test Data:Special characters !@ # $ % ^ & * ( )} { ] [\ |'; ;/,~ `
    And I fill the data for "CaptureNewRequest" with key "Data1"
    #Verify Legal Entity Name field accepts lower case characters. Test Data: Lower case char a to z
    And I fill the data for "CaptureNewRequest" with key "Data2"
    #Verify Legal Entity Name field accepts Upper case characters. Test Data: Lower case char A to Z
    And I fill the data for "CaptureNewRequest" with key "Data3"
    #Verify Legal Entity Name field accepts numbers zero to nine. Test Data: 0 to 9
    And I fill the data for "CaptureNewRequest" with key "Data4"
    #Verify Legal Entity Name field accepts .(period/full stop)
    And I fill the data for "CaptureNewRequest" with key "Data5"
    #Verify Legal Entity Name field accepts Space ()
    And I fill the data for "CaptureNewRequest" with key "Data6"
    #Verify Legal Entity Name field accepts valid characters combination of upper case, lower case, numeric o to 9, . (full stop), space
    And I fill the data for "CaptureNewRequest" with key "Data7"
    #Verify Legal Entity Name field NOT accepts more than 1024 characters. Test data : 1025 characters
    And I fill the data for "CaptureNewRequest" with key "Data8"
    #Verify Legal Entity Name field accepts less than or equal to 1024 characters. Test data : 1024, 1023 characters
    And I fill the data for "CaptureNewRequest" with key "Data9"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    #Validate the below fields ae hidden in customer details section of Review request screen
    And I validate the following fields in "Customer Details" Sub Flow
      | Label                                                           | Visible |
      | Is this entity publicly listed?                                 | false   |
      | Name of Stock Exchange                                          | false   |
      | Stock Exchange Domiciled in FAB's Recognised List of Countries? | false   |
      | Country of Stock Exchange                                       | false   |
      | Is this entity regulated?                                       | false   |
      | Regulated By                                                    | false   |
      | Regulatory ID                                                   | false   |
      | FAB Segment                                                     | false   |
      | Group name                                                      | false   |
      | SWIFT BIC                                                       | false   |
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    #Validate Country of Domicile is hidden from the screen
    And I validate the following fields in "KYC Conditions" Sub Flow
      | Label               | Visible |
      | Country of Domicile | false   |
