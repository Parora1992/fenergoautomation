#Test Case: TC_R1EPIC002PBI020_02
#PBI: R1EPIC002PBI020
#User Story ID: US068, US109, US111
#Designed by: Anusha PS
#Last Edited by: Niyaz Ahmed
Feature: Enrich Client Information (Capture Hierarchy Details>Associated Parties>Association Details)

   @TC_R1EPIC002PBI020_02
  Scenario: Validate behaviour of fields in Capture Hierarchy Details>Associated Parties>Association Details screen for expressly added associated party - part 2
    Given I login to Fenergo Application with "RM"
    #Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
    When I create new request with ClientEntityType as "Corporate" and LegalEntityrole as "Client/Counterparty"
    And I navigate to "CaptureRequestDetailsFAB" task
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYCProfileFAB" task
    When I navigate to "CaptureHierarchyDetailsGrid" task
    #Perform below step by right clicking the legal entity from hierarchy and clicking "Add Associated Party"
    And I navigate to "Associated Parties" screen for the entity
    And I fill mandatory fields
    #Perform below step by clicking "Create New LE" button
    And I navigate to "Association Details" screen of expressly added associated party
    And I validate "Is BOD?" field is not visible
    And I validate "Is Greater than 5% Shareholder?" field is not visible
    And I validate "Shareholding %" field is not visible
    And I select "Non Executive Director" for "Assoication Type" field
    And I validate "Is BOD?" field is visible
    And I validate the conditional field in "Association" section
      | Fenergo Label Name | Field Type | Visible | Editable | Mandatory | Field Defaults To |
      | Is BOD?            | Drop-down  | Yes     | Yes      | Yes       | Select...         |
    And I select "No" for "Is BOD?" field
    And I validate "Is Greater than 5% Shareholder?" field is not visible
    And I select "CEO" for "Assoication Type" field
    And I validate "Is BOD?" field is not visible
    And I validate "Is Greater than 5% Shareholder?" field is not visible
    And I select "Trustee" for "Assoication Type" field
    And I validate "Is BOD?" field is visible
    And I select "Yes" for "Is BOD?" field
    And I validate "Is Greater than 5% Shareholder?" field is visible
    And I select "Ultimate Beneficial Owner (UBO)" for "Assoication Type" field
    And I validate "Is BOD?" field is not visible
    #Play around with different combination like above steps
    #Verify Legal Entity Name field NOT accepts special characters. Test Data:Special characters !@ # $ % ^ & * ( )} { ] [\ |'; ;/,~ `
    And I fill the data for "AssociationDetails" with key "Data1"
    #Verify Legal Entity Name field accepts lower case characters. Test Data: Lower case char a to z
    And I fill the data for "AssociationDetails" with key "Data2"
    #Verify Legal Entity Name field accepts Upper case characters. Test Data: Lower case char A to Z
    And I fill the data for "AssociationDetails" with key "Data3"
    #Verify Legal Entity Name field accepts numbers zero to nine. Test Data: 0 to 9
    And I fill the data for "AssociationDetails" with key "Data4"
    #Verify Legal Entity Name field accepts .(period/full stop)
    And I fill the data for "AssociationDetails" with key "Data5"
    #Verify Legal Entity Name field accepts Space ()
    And I fill the data for "AssociationDetails" with key "Data6"
    #Verify Legal Entity Name field accepts valid characters combination of upper case, lower case, numeric o to 9, . (full stop), space
    And I fill the data for "AssociationDetails" with key "Data7"
    #Verify Legal Entity Name field NOT accepts more than 255 characters. Test data : 256 characters
    And I fill the data for "AssociationDetails" with key "Data8"
    #Verify Legal Entity Name field accepts less than 255 characters. Test data : 255 characters
    And I fill the data for "AssociationDetails" with key "Data9"
