#Test Case: TC_R1EPIC002PBI008_05
#PBI: R1EPIC002PBI008
#User Story ID: US068
#Designed by: Niyaz Ahmed
#Last Edited by: Vibhav Kumar
Feature: TC_R1EPIC002PBI008_05

  @Automation
  Scenario: Validate the 'Legal Entity Category' dropdown is filtered with relevant values (11) in Validate KYC and Regulatory data screen when 'Client Type' is selected as 'Corporate' from the Enter entity details screen (Refer lov in the PBI)
    Given I login to Fenergo Application with "RM:IBG-DNE"
    #=Select client Type as 'Corporate' in Enter Entity details screen
    #=Legal Entity Category Field is not visible in screen after R2/S4.
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    Then I store the "CaseId" from LE360
    When I complete "ReviewRequest" task
    
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    And I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    #=Then I verify "Legal Entity Category" drop-down values with ClientType as "Corporate"
    #=Legal Entity Category Field is not visible in screen after R2/S4.
    And I check that below data is not visible
    |FieldLabel						|
    |Legal Entity Category|
