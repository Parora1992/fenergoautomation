#Test Case: TC_R1EPIC002PBI032_03
#PBI: R1EPIC002PBI032
#User Story ID: OOTBF024,OOTBF025,OOTBF026,OOTBF027,OOTBF028,OOTBF029,OOTBF030, US052b, US046b, US047, US048, US049, US053a / US043, US050 / US044, US051, US05a
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Documents screen validation

  @To_be_automated @TC_R1EPIC002PBI032_03
  Scenario: Validate the following in Enrich Client Info > KYC Doc Reqts > Document Details screen
    #7 fields and 2 sections are not visible
    #behaviour of "Document Source" and behaviour of fields under "Details" and "Validity"
    Given I login to Fenergo Application with "RM"
    #Creating a legal entity with "Client Entity Type" as "NBFI" and "Legal Entity Role" as "Client/Counterparty" and "Country of Incorporation" as "AE-UNITED ARAB EMIRATES"
    When I create new request with ClientEntityType as "NBFI" and LegalEntityrole as "Client/Counterparty" and CountryOfIncorporation as "AE-UNITED ARAB EMIRATES"
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYCProfileFAB" task
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    And I click on "..." Actions button from any of the documents in the matrix
    And I navigate to "Documents" screen #by clicking "Attach Document" button 
    #Validate the following 7 fields are not visible
    And I validate "Document is Current" field is not visible
    And I validate "Document is Legible" field is not visible
    And I validate "Document is Signed and in Full" field is not visible
    And I validate "Annotated if in a language other than English" field is not visible
    And I validate "Issuer of the Document" field is not visible
    And I validate "Place of Issuance" field is not visible
    And I validate "Document Direction" field is not visible
    #Validate the following 2 sections are not visible
    And I validate "Issuance" section is not visible
    And I validate "Additional Information" section is not visible
    #Verify the below field is the first field in the Documents screen
    And I validate "Document Source" field is visible
    And I validate "Document Source" field is Mandatory
    And I validate "Document Source" field is defaulted to "Upload"
    And I validate LOVs of "Document Source" field
    #Validate if only the follows LOVs are there for "Document Source"
    Then I validate the specific LOVs for "Document Source"
      | Lovs     |
      | Upload   |
      | Fileless |
      | Existing |
    #Verify if only the following fields are visible under "Details" section in the mentioned order (this should be the first section)
    And I validate the only following fields in "Details" section
      | Fenergo Label Name             | Field Type   | Visible | Editable | Mandatory | Field Defaults To |
      | Upload File                    | Upload       | Yes     | Yes      | Yes       | NA                |
      | Document Category              | Drop-down    | Yes     | Yes      | Yes       | Select...         |
      | Document Type                  | Drop-down    | Yes     | No       | Yes       | Select...         |
      | Document Name                  | Alphanumeric | Yes     | No       | Yes       | NA                |
      | Document Identification Number | Alphanumeric | Yes     | Yes      | No        | NA                |
    #Validate if only the follows LOVs are there for "Document Category"
    Then I validate the specific LOVs for "Document Category"
      | Lovs                   |
      | AOF                    |
      | Constitutive           |
      | Authorized Signatories |
      | MISC                   |
    And I select "AOF" for "Document Category" field
    And I select "Account Opening Form" for "Document Type" field
    And I validate "Document Name" field is defaulted to "Account Opening Form"
    And I validate "Document Name" field is not-editable
    And I select "MISC" for "Document Category" field
    And I select "Others" for "Document Type" field
    And I validate "Document Name" field is defaulted to blank
    And I validate "Document Name" field is editable
    And I assert that "Document Name" accepts less than or equal to 100 characters
    And I assert that "Document Name" does not accept more than 100 characters
    And I assert that "Document Identification Number" accepts less than or equal to 100 characters
    And I assert that "Document Identification Number" does not accept more than 100 characters
    #Verify if only the following fields are visible under "Details" section in the mentioned order (this should be the second and last section)
    And I validate the only following fields in "Validity" section
      | Fenergo Label Name | Field Type | Visible | Editable | Mandatory | Field Defaults To |
      | Date of Issuance   | Date       | Yes     | Yes      | No        | NA                |
      | Date of Expiration | Date       | Yes     | Yes      | No        | NA                |
      | Effective Date     | Date       | Yes     | Yes      | No        | NA                |
    And I add document
    And I assert field names in the grid are reflecting correctly
