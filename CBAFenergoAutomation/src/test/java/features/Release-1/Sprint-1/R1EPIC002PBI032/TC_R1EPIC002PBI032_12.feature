#Test Case: TC_R1EPIC002PBI032_12
#PBI: R1EPIC002PBI032
#User Story ID: US053a / US043, US048
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Documents screen validation

  @To_be_automated @TC_R1EPIC002PBI032_10
  Scenario: Validate the behaviour of "Document Identification Number" field on entering duplicate value in all 9 screens & Validate the LOVs of Document Type field "Corporate" entitytype in all 9 screens
    Given I login to Fenergo Application with "RM"
    #Creating a legal entity with "Client Entity Type" as "NBFI" and "Legal Entity Role" as "Client/Counterparty" and "Country of Incorporation" as "AE-UNITED ARAB EMIRATES"
    When I create new request with ClientEntityType as "NBFI" and LegalEntityrole as "Client/Counterparty" and CountryOfIncorporation as "AE-UNITED ARAB EMIRATES"
    And I navigate to "CaptureRequestDetailsFAB" task
    And I navigate to "Documents" screen #by clicking "+" button
    And I select "AOF" for "Document Category" field
    And I validate LOVs of "Document Type" field
    #Refer PBI for LOV list (12 values)
    And I select "Authorized Signatories" for "Document Category" field
    And I validate LOVs of "Document Type" field
    #Refer PBI for LOV list (8 values)
    And I select "Constitutive" for "Document Category" field
    And I validate LOVs of "Document Type" field
    #Refer PBI for LOV list (16 values)
    And I select "MISC" for "Document Category" field
    And I validate LOVs of "Document Type" field
    #Refer PBI for LOV list (12 values)
    And I enter duplicate value for "Document Identification Number" field
    And I assert that the following message is diplayed along with reference to the details and location of the other (duplicate) document
      | Document Identification Number already exists |
    #Repeat the same validation for all the remaining 8 screens
    #LE > Documents > Document Details
    #Enrich Client Info > KYC Doc Reqts > Document Details
    #AML > Complete AML > Document Details
    #AML > Complete ID&V > Edit Verification > Document Details
    #Capture Request Details > Product > Document Details
    #Enrich KYC Profile > Tax Identifier > Document Details
    #Capture Request > Trading Entity > Product > Document Details
    #AML > Complete AML > Hierarchy > Add Fircosoft Screening > Assessment > Document Details
    
