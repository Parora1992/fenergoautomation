#Test Case: TC_R1EPIC002PBI015_016
#PBI: R1EPIC002PBI015
#User Story ID: US106
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora

Feature: COB 

Scenario: Validate behavior of "Entity Type" drop-down field on "LE360overview" screen of "New request" stage
	Given I login to Fenergo application with "RM" user
	When I click on "+" sign to create new request
	When I navigate to "Enter Entity details" screen
	When I complete "Enter Entity details" screen task
	When I create "CompleteRequest" by clicking on "Create Entity" button
	When I navigate to "LE360overview" screen
	When I navigate to "Customerdetailsgrid" link by clicking on "Ledetails" link present on Left hand navigation panel
	#Test Data:"Entity Type" drop-down field will be mandatory,defaults to "Non-individual" on "LE360overview" screen under "Customerdetailsgrid" link when user select "Corporate" in "Client Type" drop-down
	When I select "Corporate" in "Client Type" drop-down
	Then I can see "Entity Type" drop-down is mandatory,defaults to "Non-individual" and the field is displaying as "read only" on "LE360overview" screen
	#Test Data:"Entity Type" drop-down field will be mandatory,defaults to "Non-individual" on "LE360overview" screen under "Customerdetailsgrid" link when user select "Financial Institution(FI)" in "Client Type" drop-down
	When I select "Financial Institution(FI)" in "Client Type" drop-down
	Then I can see "Entity Type" drop-down is mandatory,defaults to "Non-individual" and the field is displaying as "read only" on "LE360overview" screen
	#Test Data:"Entity Type" drop-down field will be mandatory,defaults to "Non-individual" on "LE360overview" screen under "Customerdetailsgrid" link when user select "Non-Bank Financial Institution (NBFI)" in "Client Type" drop-down
	When I select "Non-Bank Financial Institution (NBFI)" in "Client Type" drop-down
	Then I can see "Entity Type" drop-down is mandatory,defaults to "Non-individual" and the field is displaying as "read only" on "LE360overview" screen
	#Test Data:"Entity Type" drop-down field will be mandatory,defaults to "Non-individual" on "LE360overview" screen under "Customerdetailsgrid" link when user select "PCG-Entity" in "Client Type" drop-down
	When I select "PCG-Entity" in "Client Type" drop-down
	Then I can see "Entity Type" drop-down is mandatory,defaults to "Non-individual" and the field is displaying as "read only" on "LE360overview" screen
	