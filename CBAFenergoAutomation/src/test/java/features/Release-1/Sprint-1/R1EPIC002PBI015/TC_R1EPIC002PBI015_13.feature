#Test Case: TC_R1EPIC002PBI015_011
#PBI: R1EPIC002PBI015
#User Story ID: US106
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora

Feature: COB 

Scenario: Validate mandatory field "Entity Type" drop-down on "Search for Duplicates" screen of "New request" stage
	Given I login to Fenergo application with "RM" user
	When I click on "+" sign to create new request
	When I navigate to "Enter Entity details" screen
	When I complete "Enter Entity details" screen task
	When I navigate to "Search for Duplicates" screen by clicking on "search" button
	#Test Data:Field "Entity Type" drop-down is displaying as Mandatory on "Search for duplicates" screen of "New request" stage
	Then I can see "Entity Type" drop-down is displaying as mandatory on "Search for duplicates" screen