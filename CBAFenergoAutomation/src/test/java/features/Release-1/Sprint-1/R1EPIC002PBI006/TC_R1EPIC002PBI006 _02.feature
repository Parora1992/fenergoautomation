#Test Case: TC_R1EPIC002PBI006_02
#PBI: R1EPIC002PBI006
#User Story ID: US129, US131, US133, US138, USTOM 21, USTOM 22, US128, US140, US130
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed.
Feature: TC_R1EPIC002PBI006_02

  @TobeAutomated
  Scenario: Verify the field behaviors of modified fields in KYC Conditions section of Validate KYC and Regulatory Data Screen
    #Is this entity publicly listed?
    #Name of Stock Exchange
    #Is this FAB's Recognized Stock Exchange?
    #Name of Regulatory Body
    #Regulatory ID
    #Is the entity a wholly-owned subsidiary of a parent?
    #Types of Shares (Bearer/Registered)
    #Countries of Business Operations/Economic Activity
    #Is this parent entity publicly listed?
    #Is there an AML program in place?
    Given I login to Fenergo Application with "RM"
    #Create entity with Country of Incorporation = UAE and client type = corporate
    When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    And I fill the data for "CaptureNewRequest" with key "C1"
    And I click on "Continue" button
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    And I check that below data is available
      | FieldLabel                                           | Field Type | Mandatory | Editable | Field Defaults to | Visible |
      | Is this entity publicly listed?                      | Dropdown   | Yes       | Yes      | Select...         | Yes     |
      | Regulatory ID                                        | Textbox    |           |          |                   | No      |
      | Is the entity a wholly-owned subsidiary of a parent? | Greyed Out | No        | No       | System calculated | Yes     |
      | Types of Shares (Bearer/Registered)                  | Dropdown   | Yes       | Yes      | Select...         | Yes     |
      | Countries of Business Operations/Economic Activity   | Dropdown   | Yes       | Yes      |                   | Yes     |
      | Is there an AML program in place?                    | Dropdown   |           |          |                   | No      |
    #Verfiy the field 'Name of Stock Exchange' is displayed when 'yes' is selected for the field 'Is this entity publicly listed?'
    #Test data: Is this entity publicly listed? = Yes
    And I fill the data for "ValidateKYCandRegulatoryFAB" with key "Data1"
    And I check that below data is available
      | FieldLabel                              | Field Type | Mandatory | Editable | Field Defaults to | Visible | Multiselect |
      | Name of Stock Exchange                  | Dropdown   | Yes       | Yes      | Select...         | Yes     | NO          |
      | Is this FABs Recognized Stock Exchange? | Dropdown   | Yes       | Yes      | Select...         | Yes     | NO          |
    #Verfiy the field 'Name of Stock Exchange' is displayed when 'yes' is selected for the field 'Is this parent entity publicly listed?'
    #Test data: Is this parent entity publicly listed? = Yes
    And I fill the data for "ValidateKYCandRegulatoryFAB" with key "Data1"
    And I check that below data is available
      | FieldLabel                              | Field Type | Mandatory | Editable | Field Defaults to | Visible | Multiselect |
      | Name of Stock Exchange                  | Dropdown   | Yes       | Yes      | Select...         | Yes     | NO          |
      | Is this FABs Recognized Stock Exchange? | Dropdown   | Yes       | Yes      | Select...         | Yes     | NO          |
    #Verfiy the field 'Name of Stock Exchange' is NOT displayed when 'NO' is selected for the field 'Is this entity publicly listed?'
    #Test data: Is this entity publicly listed? = No
    And I fill the data for "ValidateKYCandRegulatoryFAB" with key "Data1"
    And I check that below data is available
      | FieldLabel                              | Field Type | Mandatory | Editable | Field Defaults to | Visible |
      | Name of Stock Exchange                  | Dropdown   | No        | No       |                   | No      |
      | Is this FABs Recognized Stock Exchange? | Dropdown   | No        | No       |                   | No      |
    #Verfiy the field 'Name of Stock Exchange' is NOT displayed when 'No' is selected for the field 'Is this parent entity publicly listed?'
    #Test data: Is this parent entity publicly listed? = No
    And I fill the data for "ValidateKYCandRegulatoryFAB" with key "Data1"
    And I check that below data is available
      | FieldLabel                              | Field Type | Mandatory | Editable | Field Defaults to | Visible |
      | Name of Stock Exchange                  | Dropdown   | NO        | No       |                   | No      |
      | Is this FABs Recognized Stock Exchange? | Dropdown   | No        | No       |                   | No      |
    #Verfiy the field 'Name of Stock Exchange' is NOT displayed when 'No' is selected for the field 'Is this parent entity publicly listed?' and 'Is this entity publicly listed?'
    #Test data: Is this parent entity publicly listed? = No and Is this entity publicly listed? = NO
    And I fill the data for "ValidateKYCandRegulatoryFAB" with key "Data1"
    And I check that below data is available
      | FieldLabel                              | Field Type | Mandatory | Editable | Field Defaults to | Visible |
      | Name of Stock Exchange                  | Dropdown   | NO        | No       |                   | No      |
      | Is this FABs Recognized Stock Exchange? | Dropdown   | No        | No       |                   | No      |
    #Verfiy the field 'Name of Regulatory Body ' is not available when client type is Corporate and when 'yes' is selected for the field 'Is this Entity Regulated?'
    #Test data: Is this Entity Regulated? = Yes
    And I fill the data for "ValidateKYCandRegulatoryFAB" with key "Data1"
    And I check that below data is available
      | FieldLabel              | Field Type | Mandatory | Editable | Field Defaults to | Visible |
      | Name of Regulatory Body | Textbox    |           |          |                   | No      |
    #Verfiy the field 'Name of Regulatory Body ' is NOT displayed when 'No' is selected for the field 'Is this Entity Regulated?'
    #Test data: Is this Entity Regulated? = No
    And I fill the data for "ValidateKYCandRegulatoryFAB" with key "Data1"
    And I check that below data is available
      | FieldLabel              | Field Type | Mandatory | Editable | Field Defaults to | Visible |
      | Name of Regulatory Body | Textbox    | No        | No       |                   | No      |
    #Verfiy the field 'Is this parent entity publicly listed?' is displayed when 'No' is selected for the field 'Is the Entity Publicly Listed?'
    #Test data: Is the Entity Publicly Listed? = No
    And I fill the data for "ValidateKYCandRegulatoryFAB" with key "Data1"
    And I check that below data is available
      | FieldLabel                             | Field Type | Mandatory | Editable | Field Defaults to | Visible |
      | Is this parent entity publicly listed? | Dropdown   | Yes       | Yes      | Select...         | Yes     |
    #Verfiy the field 'Is this parent entity publicly listed?' is NOT displayed when 'Yes' is selected for the field 'Is the Entity Publicly Listed?'
    #Test data: Is the Entity Publicly Listed? = Yes
    And I fill the data for "ValidateKYCandRegulatoryFAB" with key "Data1"
    And I check that below data is available
      | FieldLabel                             | Field Type | Mandatory | Editable | Field Defaults to | Visible |
      | Is this parent entity publicly listed? | Dropdown   | NO        | No       |                   | No      |
    #Validate the 'Is this entity publicly listed?' lovs
    Then I validate the specific LOVs for "Is this entity publicly listed?"
      | Lovs |
      | Yes  |
      | No   |
    #Validate the 'Is this entity regulated?' lovs
    Then I validate the specific LOVs for "Is this entity regulated?"
      | Lovs |
      | Yes  |
      | No   |
    #Validate the 'Is this FAB's Recognized Stock Exchange?' lovs
    Then I validate the specific LOVs for "Is this FAB's Recognized Stock Exchange?"
      | Lovs |
      | Yes  |
      | No   |
    #Validate the 'Is this parent entity publicly listed?' lovs
    Then I validate the specific LOVs for "Is this parent entity publicly listed?"
      | Lovs |
      | Yes  |
      | No   |
    #Validate the 'Is there an AML program in place?' lovs
    Then I validate the specific LOVs for "Is there an AML program in place?"
      | Lovs |
      | Yes  |
      | No   |
    #Validate the 'Types of Shares (Bearer/Registered)' lovs
    Then I validate the specific LOVs for "Types of Shares (Bearer/Registered)"
      | Lovs                                               |
      | Ownership Structures Include Issuing Bearer Shares |
      | Registered Shares                                  |
    #Validate the 'Name of Stock Exchange' lovs (Refer PBI - Stock Exchange )
    And I validate the LOV of "Name of Stock Exchange" with key "lov1"
    #Validate the 'Countries of Business Operations/Economic Activity' lovs (Refer PBI - Countries )
    And I validate the LOV of "Countries of Business Operations/Economic Activity" with key "lov1"
