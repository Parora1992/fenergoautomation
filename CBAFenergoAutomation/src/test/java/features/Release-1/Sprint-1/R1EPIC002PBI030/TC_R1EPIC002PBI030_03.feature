#Test Case: TC_R1EPIC002PBI030_03
#PBI: R1EPIC002PBI030
#User Story ID: US074
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature:  TC_R1EPIC002PBI030_03

  @Automation
  Scenario: Validate "Legal Entity Source of Wealth" is renamed as "Legal Entity Source of Income & Wealth" under "Source Of Funds And Wealth Details" section for Onboarding Maker user
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    And I click on "AddAddresss" button
    When I complete "Addresses" screen with key "CountryAsUAE"
    And I click on "SaveAddress" button
    Then I can see "Legal Entity Source of Wealth" label is renamed as "Legal Entity Source of Income & Wealth" on "EnrichKYCProfile" screen
    And I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
	
		When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessment" screen with key "Low"
    
   Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task    
  
    #Validating that the case status is closed
    When I navigate to "LE360overview" screen
    When I navigate to "LEDetails" screen
    And I click on "Source Of Funds And Wealth Details" button
    # Test data- I validate "Legal Entity Source of Wealth" is renamed as "Legal Entity Source of Income & Wealth" under "Source Of Funds And Wealth Details" section on "LE360-overview" task
    Then I can see "Legal Entity Source of Wealth" label is renamed as "Legal Entity Source of Income & Wealth" on "Source Of Funds And Wealth Details" screen
    And I click on "Legal Entity Source of Income & Wealth" button to take screenshot
   