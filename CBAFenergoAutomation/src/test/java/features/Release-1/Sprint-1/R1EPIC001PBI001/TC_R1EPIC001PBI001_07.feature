#Test Case: TC_R1EPIC01PBI001_07
#PBI: R1EPIC01PBI001
#User Story ID: US013
#Designed by: Sanjeet Singh
#Last Edited by: Anusha PS
Feature: TC_R1EPIC01PBI001_07

@TC_R1EPIC01PBI001_07
Scenario: To Verify RM is able to remove product from action button in "Capture Request Details" and "Review Request" screen

	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with legal entity role as Client/Counterparty 
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty" 
	And I add a Product from "CaptureRequestDetails"
	When I expand "ProductGridExpand" and zoom out so that grid gets fully visible for "CaptureRequestDetails"
	And I assert "Status" column is visible in the Product Grid for "CaptureRequestDetails"
	And I assert "Status" column is not present as a field below the grid for "CaptureRequestDetails" 
	When I delete the Product from "CaptureRequestDetails"
	And I assert ProductSection is empty in "CaptureRequestDetails"
	
	# Add a product again
	 And I complete "CaptureNewRequest" with Key "C1" and below data 
	|Product|Relationship|
	|C1|C1|
	And I click on "Continue" button
	Then I assert that "ProductRemove" is not visible
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
