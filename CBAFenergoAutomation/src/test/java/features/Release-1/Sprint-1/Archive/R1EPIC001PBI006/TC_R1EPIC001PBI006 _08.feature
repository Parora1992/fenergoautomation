Feature: TC_R1EPIC001PBI006 _08

Scenario: Verify the FLOD VP user is able to search a case using 'Assigned To'.
	
	Given I login to Fenergo Application with "FLOD VPUser" 
	When I navigate to "CaseSearch" screen
	#Test Data: Assigned To = Any FLOD VP user
	And I fill in data in CaseSearch screen
	When I click on Search button in CaseSearch screen
	#User should be able to view all cases assigned to the FLOD VP user
	Then I validate the search grid data
