#Test Case: TC_R1EPIC01PBI001_07
#PBI: R1EPIC01PBI008
#User Story ID: US028
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora

Feature: COB 

Scenario: Validate the behavior of "Tax Value field" for TAX identifier Type other than "VAT ID"
	Given I login to Fenergo application with "RM" user
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "ClientCounterparty"
	When I complete to "CaptureRequestDetailsGrid" task
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	When I complete the "ValidateKYCandRegulatoryGrid" task
	When I login to Fenergo application with "KYCmaker" user
	When I search for the "CaseId" 
	Then I navigate to "EnrichClientProfileGrid" task
	#Test Data: Tax Type: SSN (other than VAT ID)
	When I add a "TAX Type" as "SSN" on "AddTaxIdentifier" screen
	Then I can see "TaxIdentifierValue" is displaying as mandatory 
