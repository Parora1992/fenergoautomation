Feature: TC_R1EPIC001PBI006 _04

Scenario: Verify the Onboarding Checker user is able to search a case using 'Assigned To'.
	
	Given I login to Fenergo Application with "OnboardingCheckerUser" 
	When I navigate to "CaseSearch" screen
	#Test Data: Assigned To = Any Onboarding Checker user
	And I fill in data in CaseSearch screen
	When I click on Search button in CaseSearch screen
	#User should be able to view all cases assigned to the Onboarding Checker user
	Then I validate the search grid data
