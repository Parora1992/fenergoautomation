#Test Case: TC_R1EPIC01PBI011_06
#PBI: R1EPIC01PBI011
#User Story ID: US031
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC01PBI011_06 

@Automation
Scenario: "Accounts" subflow is displaying as hidden on "Validate KYC and Regulatory Data" task  on "Validate data" stage
	Given I login to Fenergo Application with "RM:IBG-DNE"
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	And I complete "CaptureNewRequest" with Key "C1" and below data 
	|Product | Relationship |
	|C1			 | C1					  |
	And I add a Product from "CaptureRequestDetails"
	And I click on "Continue" button
	And I store the "CaseId" from LE360
	When I complete "ReviewRequest" task 
	
	Given I login to Fenergo Application with "KYCMaker: Corporate"
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	Then I can see "Accounts" subflow is hidden
	