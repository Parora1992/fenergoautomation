#Test Case: TC_WAC_Corporate_32
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Priyanka
#Last Edited by: Priyanka
Feature: TC_WAC_Corporate_32
@Automation
  Scenario: Derive Overall Risk Rating as'Medium' by adding multiple products with different risk rating
    #Refer to TC40 in the WAC Corp Data Sheet
     Given I login to Fenergo Application with "RM:IBG-DNE" 
	When I complete "NewRequest" screen with key "RA40" 
	When I complete "Product" screen with key "Bancassurance" 
	When I complete "Product" screen with key "AssetFinance"
	When I complete "Product" screen with key "FiduciaryAccount"
	When I complete "Product" screen with key "CapitalMarketAdvisory"
	And I complete "CaptureNewRequest" with Key "RA40" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 

	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId" 

	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "RA40" 
	And I click on "SaveandCompleteforValidateKYC" button 
    When I navigate to "EnrichKYCProfileGrid" task 
##	#	When I add a "AnticipatedTransactionActivity" from "EnrichKYC" 
    When I complete "AddAddressFAB" task 
	When I complete "EnrichKYC" screen with key "RA40" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	When I complete "AssociationDetails" screen with key "Director" 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Individual" 
	When I complete "AssociationDetails" screen with key "DirectorIndividual" 
	When I complete "CaptureHierarchyDetails" task 
##	
	When I navigate to "KYCDocumentRequirementsGrid" task 
###	###	Then I compare the list of documents should be same as "COBDocument" for ClientType "Corporate" 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	
	When I navigate to "CompleteAMLGrid" task 
	And I navigate to "Navigate to Legal Entity" screen of the added "NonIndividual" AssociatedParty 
	When I complete "LEDetailsNonIndividual" screen with key "UAE" 
	When I search for the "CaseId" 
	When I navigate to "CompleteAMLGrid" task 
	And I navigate to "Navigate to Legal Entity" screen of the added "Individual" AssociatedParty 
	When I complete "LEDetailsIndividual" screen with key "UAE" 
	When I search for the "CaseId" 
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Google" by rightclicking 
	When I Initiate "Google" by rightclicking for "2" associated party 
	And I complete "Google" from assessment grid with Key "RA40" 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task 
	And I validate that the RiskCategory is "Medium-Low" 
#	When I complete "RiskAssessment" task 