#Test Case: TC_WAC_Corporate_19
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_WAC_Corporate_19 

Scenario:
RR-Derive the overrall risk rating as Medium (COB overall risk rating is Low) 
#Refer to TC19 in the WAC Corp Data Sheet
	Given I login to Fenergo Application with "RM:IBG-DNE" 
	When I complete "NewRequest" screen with key "RA32" 
	When I complete "Product" screen with key "TrustAccount" 
	And I complete "CaptureNewRequest" with Key "RA34" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "RA34" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	##	#	When I add a "AnticipatedTransactionActivity" from "EnrichKYC" 
	When I complete "AddAddressFAB" task 
	When I complete "EnrichKYC" screen with key "RA34" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	When I complete "AssociationDetails" screen with key "Director" 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Individual" 
	When I complete "AssociationDetails" screen with key "DirectorIndividual" 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	##	Then I compare the list of documents should be same as "COBDocument" for ClientType "Corporate" 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	And I navigate to "Navigate to Legal Entity" screen of the added "NonIndividual" AssociatedParty 
	When I complete "LEDetailsNonIndividual" screen with key "RA34" 
	When I search for the "CaseId" 
	When I navigate to "CompleteAMLGrid" task 
	And I navigate to "Navigate to Legal Entity" screen of the added "Individual" AssociatedParty 
	When I complete "LEDetailsIndividual" screen with key "UAE" 
	When I search for the "CaseId" 
	When I navigate to "CompleteAMLGrid" task 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task
	And I validate that the RiskCategory is "Medium-Low"
