#Test Case: TC_WAC_Corporate_36
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Priyanka
#Last Edited by: Priyanka
Feature: TC_WAC_Corporate_36

  Scenario: 1. Derive Overall Risk Rating as 'High' by adding multiple countries with different risk rating
    #Refer to TC44 in the WAC Corp Data Sheet