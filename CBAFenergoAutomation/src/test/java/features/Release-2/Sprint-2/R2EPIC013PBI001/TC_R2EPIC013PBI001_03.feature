#Test Case: TC_R2EPIC013PBI001_03
#PBI: R2EPIC013PBI001
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora

Feature: TC_R2EPIC013PBI001_03:GLCMS
	@Automation
  Scenario: Validate all the below options(Navigate, view task) for "Waiting for UID from GLCMS" task under case details task screen
  	#is displayed as completed when fenergo receives  GLCMS UID from GLCMS through Mule Middleware system
     Given I login to Fenergo Application with "RM:IBG-DNE"
		When I complete "NewRequest" screen with key "Corporate"
		And I complete "CaptureNewRequest" with Key "C1" and below data
				| Product | Relationship |
				| C1      | C1           |
		And I click on "Continue" button
		When I complete "ReviewRequest" task
		Then I store the "CaseId" from LE360
		Given I login to Fenergo Application with "KYCMaker: Corporate"
		When I search for the "CaseId"
		When I navigate to "ValidateKYCandRegulatoryGrid" task
		When I complete "ValidateKYC" screen with key "C1"
		And I click on "SaveandCompleteforValidateKYC" button
		When I navigate to "EnrichKYCProfileGrid" task
		When I complete "AddAddressFAB" task
		When I complete "EnrichKYC" screen with key "C1"
		And I click on "SaveandCompleteforEnrichKYC" button
		When I navigate to "CaptureHierarchyDetailsGrid" task
		When I complete "CaptureHierarchyDetails" task
		When I navigate to "KYCDocumentRequirementsGrid" task
		When I add a "DocumentUpload" in KYCDocument
		Then I complete "KYCDocumentRequirements" task
		
		When I navigate to "CompleteAMLGrid" task
		Then I complete "CompleteAML" task
		
		When I navigate to "CompleteID&VGrid" task
		When I complete "CompleteID&V" task
		
		When I navigate to "CompleteRiskAssessmentGrid" task 
		When I complete "RiskAssessment" task 
		
		Then I login to Fenergo Application with "RM:IBG-DNE"
		When I search for the "CaseId"
		When I navigate to "ReviewSignOffGrid" task
		When I complete "ReviewSignOff" task
		Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
		When I search for the "CaseId"
		When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
		When I complete "ReviewSignOff" task
		Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
		When I search for the "CaseId"
		When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
		When I complete "ReviewSignOff" task
		Then I login to Fenergo Application with "BUH:IBG-DNE"
		When I search for the "CaseId"
		When I navigate to "BHUReviewandSignOffGrid" task
		When I complete "ReviewSignOff" task
		Then I see "WaitingForGLCMSUIDGrid" task is generated
		And I take a screenshot
		
		Then I login to Fenergo Application with "KYCMaker: Corporate"
		When I search for the "CaseId"
		And I take a screenshot
		
		Then I see "WaitingForGLCMSUIDGrid" task is generated
		And I validate "Waiting for UID from GLCMS" task is Assigned to "CIB KYC Maker"
		And I complete "Waiting for UID from GLCMS" task from Actions button
    
    
    

    
    #Given I login to Fenergo Application with "RM:IBG-DNE"
    #When I complete "NewRequest" screen with key "Corporate"
    #And I complete "CaptureNewRequest" with Key "C1" and below data
      #| Product | Relationship |
      #| C1      | C1           |
    #And I click on "Continue" button
    #When I complete "ReviewRequest" task
    #Then I store the "CaseId" from LE360
    #Given I login to Fenergo Application with "KYCMaker: Corporate"
    #When I search for the "CaseId"
    #When I navigate to "ValidateKYCandRegulatoryGrid" task
    #When I complete "ValidateKYC" screen with key "C1"
    #And I click on "SaveandCompleteforValidateKYC" button
    #Given I login to Fenergo Application with "KYCMaker: Corporate"
    #When I search for the "CaseId"
    #When I navigate to "EnrichKYCProfileGrid" task
    #When I complete "EnrichKYC" screen with key "C1"
    #And I click on "SaveandCompleteforEnrichKYC" button
    #When I navigate to "CaptureHierarchyDetailsGrid" task
    #When I complete "CaptureHierarchyDetails" task
    #Then I login to Fenergo Application with "Onboarding Maker"
    #Given I login to Fenergo Application with "KYCMaker: Corporate"
    #When I search for the "CaseId"
    #When I navigate to "KYCDocumentRequirementsGrid" task
    #When I add a "DocumentUpload" in KYCDocument
    #Then I complete "KYCDocumentRequirements" task
    #When I navigate to "CompleteAMLGrid" task
    #When I Initiate "Fircosoft" by rightclicking
    #And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    #And I click on "SaveandCompleteforAssessmentScreen1" button
    #Then I complete "CompleteAML" task
    #When I navigate to "CompleteID&VGrid" task
    #When I complete "CompleteID&V" task
    #When I navigate to "CaptureRiskCategoryGrid" task
    #Validate Risk category as 'Low'
    #Then I Select Risk category as 'Low'
    #And I complete "RiskAssessmentFAB" task
    #Then I login to Fenergo Application with "RM:IBG-DNE"
    #When I search for the "CaseId"
    #When I navigate to "ReviewSignOffGrid" task
    #When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydKYC"
    #Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    #When I search for the "CaseId"
    #When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    #When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydAVP"
    #Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    #When I search for the "CaseId"
    #When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    #When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "BusinessUnitHead"
    #Then I login to Fenergo Application with "BUH:IBG-DNE"
    #When I search for the "CaseId"
    #When I navigate to "BHUReviewandSignOffGrid" task
    #When I complete "ReviewSignOff" task
    #When I navigate to 'CaseDetails' task screen
    #Test-data: Validate "Waiting for UID from GLCMS" task is generated
    #Then I see "Waiting for UID from GLCMS" task is generated
    #Test-data: Validate "Waiting for UID from GLCMS" task is Assigned to 'KYCMaker: Corporate'
    #And I validate "Waiting for UID from GLCMS" task is Assigned to 'KYCMaker: Corporate'
    #When I login to Fenergo Application with "KYCMaker: Corporate"
#		#Test-data: Validate the status of "Waiting for UID from GLCMS" task is displaying as 'Completed' in task grid
    #Then I see 'Casedetails' task screen is auto-refreshed and "Waiting for T24 CIF ID from GLCMS" task status is updated as completed  
    #Test-data: Validate the below context value under options button
    #When I validate below context value under options button
    #|Navigate|
    #|View task|
    #Test-data: Validate user is able to navigate to "Waiting for T24 CIF ID from GLCMS" task using 'navigate' button
    #When I click on 'Navigate' option displying under options button
    #Then I see "Waiting for T24 CIF ID from GLCMS" task screen is displayed
    #Test-data: Validate user is able to navigate to "Waiting for T24 CIF ID from GLCMS" task using 'View Task' button
    #When I click on 'View Task' option displying under options button
    #Then I see "Waiting for T24 CIF ID from GLCMS" task screen is displayed
    
    
    
   
