#Test Case: TC_R2EPIC024PBI011_02
#PBI:R2EPIC024PBI011
#User Story ID:Back_Log_001
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2EPIC024PBI011_02

Scenario: #Validate If a RM/GRM is tagged to 3 DAO code on Add Relationship screen of capture request details task , the user should see 3 entry of the Same RM, each with different DAO code for COB WF.
#Validate when a RM and DAO Code is selected from the existing relationship grid , only selected DAO Code should be displayed in the 'User Details' section on 'Add Relationship' screen of capture request details task for COB WF
#Validate a new column in the relationship sub-flow grid is added to display the selected DAO Code against RM on capture request details screen of COB workflow for COB WF.
#Validate a new column in the relationship sub-flow grid is displaying on LE 360-LEverified details task screen 
 Given I login to Fenergo Application with "Admin"
 When I click on options button and click 'security' option
 When I navigate to 'user management' grid by clicking on 'user management' option 
 When I expand 'Advanced search' grid
 When I select Role group as 'relationship Manager'
 When I select Multiple values (3) from 'DAO code' drop-down to single Relationship manager
 When I save the details
 When I login to Fenergo Application with "RM:IBG-DNE"
 When I complete "NewRequest" screen with key "Corporate"
 When I navigate "CaptureNewRequest" task
 When I click on Plus button displaying at the top of 'Relationship' grid    
 When I navigate to 'Addrelationship' grid
 #Validate If a RM/GRM is tagged to 3 DAO code on Add Relationship screen
 Then I can see single Relationship manager is tagged with 3 'DAO codes'
 And I can see 3 entry of the Same RM each with different DAO code
 
 #Validate when a RM and DAO Code is selected from the existing relationship grid , only selected DAO Code should be displayed in the 'User Details' section on 'Add Relationship' screen of capture request details task 
 When I select one of the value among 3 DAO code 
 The I validate selected DAO Code is displaying in the 'User Details' grid   
 
 #Validate a new column in the relationship sub-flow grid is added to display the selected DAO Code against RM on capture request details screen
 When I select values from 'Relationship Type' and 'Relationship Status' drop-down 
 When I save the details
 When I navigate back to 'CaptureRequestdetails' task grid
 Then I validate a new column in the relationship sub-flow grid is added to display the selected DAO Code against relationship manager
And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to 'CaptureFABreferennces' task
    Then I complete 'CaptureFABreferennces' task 
    And I validate case status is updated as 'closed'
		#Validate a new column in the relationship sub-flow grid is displaying on LE 360-LEverified details task screen 
 		When I navigate to LE360-LEdetails screen
 		Then I Validate a new column in the relationship sub-flow grid is displaying
 		
 		