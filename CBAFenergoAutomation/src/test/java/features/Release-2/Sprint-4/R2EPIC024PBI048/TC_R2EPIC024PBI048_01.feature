#Test Case: TC_R2EPIC024PBI048_01
#PBI:R2EPIC024PBI048
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Vibhav Kumar
Feature: TC_R2EPIC024PBI048_01
@Automation
  Scenario: Validate 'SWIFT Address' field is not visible on enrich KYC profile screen for COB workflow for client type 'Corporate'
   #Validate 'SWIFT Address' field is not visible on review/edit client data screen for Regular review workflow for client type 'PCG-entity'
   #Validate 'SWIFT Address' field is not visible on Update customer details screen for LEM workflow for client type 'BBG'
  
    Given I login to Fenergo Application with "RM:IBG-DNE" 
     When I complete "NewRequest" screen with key "Corporate" 
      And I complete "CaptureNewRequest" with Key "C1" and below data 
      | Product | Relationship | 
      | C1      | C1           | 
      And I click on "Continue" button 
     When I complete "ReviewRequest" task 
     Then I store the "CaseId" from LE360 
  
    Given I login to Fenergo Application with "SuperUser" 
     When I search for the "CaseId" 
     Then I store the "CaseId" from LE360 
     When I navigate to "ValidateKYCandRegulatoryGrid" task 
     When I complete "ValidateKYC" screen with key "C1" 
      And I click on "SaveandCompleteforValidateKYC" button 
     When I navigate to "EnrichKYCProfileGrid" task 
     Then I check that below data is not visible
      | FieldLabel    | 
      | SWIFT Address | 
      And I click on "SaveForLaterEnrichKYC" button 
  
     When I navigate to "LE360overview" screen
      And I click on "LEDetails" button
     Then I check that below data is not visible
      | FieldLabel    | 
      | SWIFT Address | 
  
  @Automation
  Scenario: Validate 'SWIFT Address' field is not visible on enrich KYC profile screen for COB workflow for client type 'PCG' 
    Given I login to Fenergo Application with "RM:PCG" 
     When I complete "NewRequest" screen with key "PCG"
      And I complete "CaptureNewRequest" with Key "PCG-Entity" and below data 
      | Product | Relationship | 
      | C1      | C1           | 
     And I click on "Continue" button 
     When I complete "ReviewRequest" task 
     Then I store the "CaseId" from LE360 
  
    Given I login to Fenergo Application with "SuperUser" 
     When I search for the "CaseId"  
     When I navigate to "ValidateKYCandRegulatoryGrid" task 
     When I complete "ValidateKYC" screen with key "LowRiskPCGEntity" 
      And I click on "SaveandCompleteforValidateKYC" button 
  
     When I navigate to "EnrichKYCProfileGrid" task  
     When I complete "AddAddressFAB" task 
     Then I check that below data is not visible
      | FieldLabel    | 
      | SWIFT Address | 
     When I complete "EnrichKYC" screen with key "LowRiskPCGEntity" 
      And I click on "SaveandCompleteforEnrichKYC" button
  
     When I navigate to "CaptureHierarchyDetailsGrid" task 
     When I add AssociatedParty by right clicking 
     When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
     When I complete "AssociationDetails" screen with key "Director" 
     When I complete "CaptureHierarchyDetails" task 
  
     When I navigate to "KYCDocumentRequirementsGrid" task 
     When I add a "DocumentUpload" in KYCDocument 
     Then I complete "KYCDocumentRequirements" task 
  
     When I navigate to "CompleteAMLGrid" task 
     When I Initiate "Fircosoft" by rightclicking 
      And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
     Then I complete "CompleteAML" task 
  
     When I navigate to "CompleteID&VGrid" task 
     When I complete "CompleteID&V" task 
  
     When I navigate to "CompleteRiskAssessmentGrid" task 
     When I complete "RiskAssessment" task
  
    Given I login to Fenergo Application with "SuperUser" 
     When I search for the "CaseId" 	
  
     When I navigate to "ReviewSignOffGrid" task 
     When I complete "ReviewSignOff" task 
  
     When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
     When I complete "ReviewSignOff" task 
  
     When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
     When I complete "ReviewSignOff" task 
  
     When I navigate to "BHUReviewandSignOffGrid" task 
     When I complete "ReviewSignOff" task 
  
      And I complete "Waiting for UID from GLCMS" task from Actions button 
     When I navigate to "CaptureFabReferencesGrid" task 
     When I complete "CaptureFABReferencesCIFId" task 
     When I complete "CaptureFABReferences" task 
      And I assert that the CaseStatus is "Closed"
  
      And I initiate "Regular Review" from action button 
  
     When I complete "CloseAssociatedCase" task 
     When I navigate to "ValidateKYCandRegulatoryGrid" task 
     When I complete "ValidateKYC" screen with key "C1" 
      And I click on "SaveandCompleteforValidateKYC" button 
     When I navigate to "ReviewRequestGrid" task 
     When I complete "RRReviewRequest" task 
     When I navigate to "Review/EditClientDataTask" task 
     Then I check that below data is not visible
      | FieldLabel    | 
      | SWIFT Address | 
      And I click on "SaveForLaterEnrichKYC" button 
  
     When I navigate to "LE360overview" screen
      And I click on "LEDetails" button
     Then I check that below data is not visible
      | FieldLabel    | 
      | SWIFT Address | 
  
  @Automation
  Scenario: Validate 'SWIFT Address' field is not visible on enrich KYC profile screen for COB workflow for client type 'BBG' 
    Given I login to Fenergo Application with "RM:BBG" 
     When I complete "NewRequest" screen with key "BBG"
      And I complete "CaptureNewRequest" with Key "LowRiskBBG" and below data 
      | Product | Relationship | 
      | C1      | C1           | 
     And I click on "Continue" button 
     When I complete "ReviewRequest" task 
     Then I store the "CaseId" from LE360 
  
    Given I login to Fenergo Application with "SuperUser" 
     When I search for the "CaseId"  
     When I navigate to "ValidateKYCandRegulatoryGrid" task 
     When I complete "ValidateKYC" screen with key "LowRiskBBG" 
      And I click on "SaveandCompleteforValidateKYC" button 
  
     When I navigate to "EnrichKYCProfileGrid" task  
     When I complete "AddAddressFAB" task 
     Then I check that below data is not visible
      | FieldLabel    | 
      | SWIFT Address | 
     When I complete "EnrichKYC" screen with key "LowRiskBBG" 
      And I click on "SaveandCompleteforEnrichKYC" button
  
     When I navigate to "CaptureHierarchyDetailsGrid" task 
     When I add AssociatedParty by right clicking 
     When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
     When I complete "AssociationDetails" screen with key "Director" 
     When I complete "CaptureHierarchyDetails" task 
  
     When I navigate to "KYCDocumentRequirementsGrid" task 
     When I add a "DocumentUpload" in KYCDocument 
     Then I complete "KYCDocumentRequirements" task 
  
     When I navigate to "CompleteAMLGrid" task 
     When I Initiate "Fircosoft" by rightclicking 
      And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
     Then I complete "CompleteAML" task 
  
     When I navigate to "CompleteID&VGrid" task 
     When I complete "CompleteID&V" task 
  
     When I navigate to "CompleteRiskAssessmentGrid" task 
     When I complete "RiskAssessment" task
  
    Given I login to Fenergo Application with "SuperUser" 
     When I search for the "CaseId" 	
  
     When I navigate to "ReviewSignOffGrid" task 
     When I complete "ReviewSignOff" task 
  
     When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
     When I complete "ReviewSignOff" task 
  
     When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
     When I complete "ReviewSignOff" task 
  
     When I navigate to "BHUReviewandSignOffGrid" task 
     When I complete "ReviewSignOff" task 
  
      And I complete "Waiting for UID from GLCMS" task from Actions button 
     When I navigate to "CaptureFabReferencesGrid" task 
     When I complete "CaptureFABReferencesCIFId" task 
     When I complete "CaptureFABReferences" task 
      And I assert that the CaseStatus is "Closed"
  
      And I initiate "Maintenance Request" from action button  
      And I complete "MaintenanceRequest" screen with key "KYCData" 
  
      And I navigate to "CaptureProposedChangesGrid" task
  		And I click on "SaveandCompleteCaptureProposedChanges" button 
  
      And I navigate to "UpdateCusotmerDetailsGrid" task 
     Then I check that below data is not visible
      | FieldLabel    | 
      | SWIFT Address | 
  
     When I navigate to "LE360overview" screen
      And I click on "LEDetails" button
     Then I check that below data is not visible
      | FieldLabel    | 
      | SWIFT Address | 
 
 
      
    
    
    
    
  #================= Below steps are written for manual testing  
    #
    #Given I login to Fenergo Application with "RM:IBG-DNE"
    #When I complete "NewRequest" screen with key "Corporate"
    #When I complete "Product" screen with key "C1"
    #And I complete "CaptureNewRequest" with Key "C1"
      #| Product | Relationship |
      #| C1      | C1           |
    #And I click on "Continue" button
    #When I complete "ReviewRequest" task
    #When I navigate to "ValidateKYCandRegulatoryGrid" task
    #When I complete "ValidateKYC" screen with key "C1"
    #And I click on "SaveandCompleteforValidateKYC" button
    #When I navigate to "EnrichKYCProfileGrid" task
    #Validate 'SWIFT Address' field is not visible on enrich KYC profile
    #When I navigate to customer details sub-flow
    #Then I validate 'SWIFT Address' field is not visible
    #Validate 'SWIFT Address' field is not visible on LE360-LEdetails screen
    #When I navigate to 'LE360-LEdetails' task grid
    #And I 'SWIFT Address' field is not visible on LE360-LEdetails screen
    # Validate 'SWIFT Address' field is not visible on review/edit client data screen for Regular review workflow for client type 'PCG-entity'
    #Pre-requisite: Complete COB case with client type 'PCG-entity'
    #Initiate Regular Review
    #When I navigate to LE360 task screen
    #And I initiate "Regular Review" from action button
    #	Then I navigate to "CloseAssociatedCasesGrid" task
    #When I complete "CloseAssociatedCase" task
    #Then I store the "CaseId" from LE360
    #When I navigate to "ValidateKYCandRegulatoryGrid" task
    #When I complete "ValidateKYC" screen with key "C1"
    #And I click on "SaveandCompleteforValidateKYC" button
    #When I navigate to "ReviewRequestGrid" task
    #When I complete "ReviewRequestGrid" task
    #When I navigate to review/edit client data task screen
    # Validate 'SWIFT Address' field is not visible on review/edit client data screen
    #Then I Validate 'SWIFT Address' field is not visible on review/edit client data screen
    #Validate 'SWIFT Address' field is not visible on LE360-LEdetails screen
    #When I navigate to 'LE360-LEdetails' task grid
    #And I 'SWIFT Address' field is not visible on LE360-LEdetails screen
    #
    # Validate 'SWIFT Address' field is not visible on Update customer details screen for LEM workflow for client type 'BBG'
    #Pre-requisite: Complete COB case with client type 'BBG'
 #		When I navigate to LE360 task screen
 #		And I initiate "Maintenance Request" from action button
 #		When I select 'LE details' from Area drop-down, 'KYC data and details' from LE details changes drop-down and submit the details
    #And I click on "CreateLEMSubmit" button
    # Verify LEM case has been triggered
    #Verify Non-material workflow is triggered        
    #Then I see "CaptureProposedChangesGrid" task is generated
    #And I navigate to "CaptureProposedChangesGrid" task
    #When I complete "CaptureProposedChanges" screen
    #And I click on "SaveandCompleteCaptureProposedChanges" button
    #And I navigate to "UpdateCusotmerDetailsGrid" task
    #Validate 'SWIFT Address' field is not visible on Update customer details screen
    #And I Validate 'SWIFT Address' field is not visible on Update customer details screen
    #When I complete "UpdateCustomerDetails" screen
    #And I click on "SaveandCompleteUpdateCustomerDetails" button
#		#Validate 'SWIFT Address' field is not visible on LE360-LEdetails screen
    #When I navigate to 'LE360-LEdetails' task grid
    #And I 'SWIFT Address' field is not visible on LE360-LEdetails screen
    
    