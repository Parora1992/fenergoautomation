#Test Case: TC_R2EPIC024PBI048_03
#PBI:R2EPIC024PBI048
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Vibhav Kumar
Feature: TC_R2EPIC024PBI048_03
@Automation
  Scenario: Validate 'SWIFT Address' field is visible and non-mandatory on enrich KYC profile screen for COB workflow for client type 'NBFI'
     #Validate 'SWIFT Address' field is visible, editable and non-non-mandatory on review/edit client data screen for Regular review workflow for client type 'NBFI'
     #Validate 'SWIFT Address' field is visible, editable and non-mandatory on Update customer details screen for LEM workflow for client type 'NBFI'
     #Validate 'SWIFT Address' field is visible, editable and non-mandatory on enrich KYC profile screen for Lite KYC workflow for client type 'NBFI'
    Given I login to Fenergo Application with "RM:NBFI"
    When I complete "NewRequest" screen with key "NBFI"
    And I complete "CaptureNewRequest" with Key "NBFI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "NBFI"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
     Then I validate the following fields in "Customer Details" Sub Flow
      | Label           | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | SWIFT Address   | TextBox   | true    | false    | NA        | Select...  | 
    When I select "ERERRR24" for "TextBox" field "SWIFT Address"
      And I click on "SaveForLaterCaptureRequestDetails" button 
  
     When I navigate to "LE360overview" screen
      And I click on "LEDetails" button
     Then I check that below data is visible
      | FieldLabel    | 
      | SWIFT Address | 
 
 @Automation   
 Scenario: Validate 'SWIFT Address' field is visible, editable and non-mandatory on enrich KYC profile screen for Lite KYC workflow for client type 'NBFI'   
    Given I login to Fenergo Application with "RM:NBFI"
    When I complete "NewRequest" screen with key "LiteKYC-NBFI"
    And I complete "CaptureNewRequest" with Key "LiteKYC-NBFI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "LiteKYC-NBFI"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
   Then I validate the following fields in "Customer Details" Sub Flow
      | Label           | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | SWIFT Address   | TextBox   | true    | false    | NA        | Select...  | 
    When I select "ERERRR24" for "TextBox" field "SWIFT Address"
    And I click on "SaveForLaterEnrichKYC" button  
    When I navigate to "LE360overview" screen
      And I click on "LEDetails" button
     Then I check that below data is visible
      | FieldLabel    | 
      | SWIFT Address |  
  
  @Automation 
  Scenario: Validate 'SWIFT Address' field is visible, editable and non-non-mandatory on review/edit client data screen for Regular review workflow for client type 'NBFI' 
   Given I login to Fenergo Application with "RM:NBFI"
    When I complete "NewRequest" screen with key "NBFI"
    And I complete "CaptureNewRequest" with Key "NBFI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "SuperUser"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "NBFI"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddressFAB" task
    When I complete "EnrichKYC" screen with key "NBFI"
    And I click on "SaveandCompleteforEnrichKYC" button
    
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking 
		When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
		When I complete "AssociationDetails" screen with key "Director" 
    When I complete "CaptureHierarchyDetails" task
    
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task 
		When I Initiate "Fircosoft" by rightclicking 
		And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
		Then I complete "CompleteAML" task 
    
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    
    When I navigate to "CompleteRiskAssessmentGrid" task 
		When I complete "RiskAssessment" task 
    
	  When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    
		When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task  

    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task

    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    And I complete "Waiting for UID from GLCMS" task from Actions button
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    
      And I initiate "Regular Review" from action button 
  
     When I complete "CloseAssociatedCase" task 
     When I navigate to "ValidateKYCandRegulatoryGrid" task  
      And I click on "SaveandCompleteforValidateKYC" button 
     When I navigate to "ReviewRequestGrid" task 
     When I complete "RRReviewRequest" task 
     When I navigate to "Review/EditClientDataTask" task 
     Then I validate the following fields in "Customer Details" Sub Flow
      | Label           | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | SWIFT Address   | TextBox   | true    | false    | NA        | Select...  | 
      And I click on "SaveForLaterEnrichKYC" button 
  
     When I navigate to "LE360overview" screen
      And I click on "LEDetails" button
     Then I check that below data is visible
      | FieldLabel    | 
      | SWIFT Address | 
   
   
   @Automation 
  Scenario: Validate 'SWIFT Address' field is visible, editable and non-mandatory on Update customer details screen for LEM workflow for client type 'NBFI' 
   Given I login to Fenergo Application with "RM:NBFI"
    When I complete "NewRequest" screen with key "NBFI"
    And I complete "CaptureNewRequest" with Key "NBFI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "SuperUser"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "NBFI"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddressFAB" task
    When I complete "EnrichKYC" screen with key "NBFI"
    And I click on "SaveandCompleteforEnrichKYC" button
    
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking 
		When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
		When I complete "AssociationDetails" screen with key "Director" 
    When I complete "CaptureHierarchyDetails" task
    
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task 
		When I Initiate "Fircosoft" by rightclicking 
		And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
		Then I complete "CompleteAML" task 
    
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    
    When I navigate to "CompleteRiskAssessmentGrid" task 
		When I complete "RiskAssessment" task 
    
	  When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    
		When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task  

    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task

    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task  
   
    Then I login to Fenergo Application with "SuperUser"
    When I search for the "CaseId"
    And I complete "Waiting for UID from GLCMS" task from Actions button
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
   
     And I initiate "Maintenance Request" from action button  
      And I complete "MaintenanceRequest" screen with key "KYCData" 
  
      And I navigate to "CaptureProposedChangesGrid" task
  		And I click on "SaveandCompleteCaptureProposedChanges" button 
  
      And I navigate to "UpdateCusotmerDetailsGrid" task 
     Then I validate the following fields in "Customer Details" Sub Flow
      | Label           | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | SWIFT Address   | TextBox   | true    | false    | NA        | Select...  |  
  
     When I navigate to "LE360overview" screen
      And I click on "LEDetails" button
     Then I check that below data is visible
      | FieldLabel    | 
      | SWIFT Address | 
    
    
    
    
    
    
    
    #==========Below steps are written for manual testing
    #Given I login to Fenergo Application with "RM:NBFI"
    #When I complete "NewRequest" screen with key "FI"
    #When I complete "Product" screen with key "C1"
    #And I complete "CaptureNewRequest" with Key "C1"
      #| Product | Relationship |
      #| C1      | C1           |
    #And I click on "Continue" button
    #When I complete "ReviewRequest" task
    #When I navigate to "ValidateKYCandRegulatoryGrid" task
    #When I complete "ValidateKYC" screen with key "C1"
    #And I click on "SaveandCompleteforValidateKYC" button
    #When I navigate to "EnrichKYCProfileGrid" task
    #Validate 'SWIFT Address' field is visible, editable and non-mandatory on enrich KYC profile
    #When I navigate to customer details sub-flow
    #Then I validate 'SWIFT Address' field is displaying as editable and non-mandatory
    #Validate 'SWIFT Address' field is visible on LE360-LEdetails screen
    #When I navigate to 'LE360-LEdetails' task grid
    #And I 'SWIFT Address' field is visible on LE360-LEdetails screen
    # Validate 'SWIFT Address' field is visible, editable and non-mandatory on review/edit client data screen for Regular review workflow for client type 'FI'
    #Pre-requisite: Complete COB case with client type 'NBFI'
    #Initiate Regular Review
    #When I navigate to LE360 task screen
    #And I initiate "Regular Review" from action button
    #	Then I navigate to "CloseAssociatedCasesGrid" task
    #When I complete "CloseAssociatedCase" task
    #Then I store the "CaseId" from LE360
    #When I navigate to "ValidateKYCandRegulatoryGrid" task
    #When I complete "ValidateKYC" screen with key "C1"
    #And I click on "SaveandCompleteforValidateKYC" button
    #When I navigate to "ReviewRequestGrid" task
    #When I complete "ReviewRequestGrid" task
    #When I navigate to review/edit client data task screen
    # Validate 'SWIFT Address' field is visible,editable and non-mandatory on review/edit client data screen
    #Then I Validate 'SWIFT Address' field is visible,editable and non-mandatory on review/edit client data screen
    #Validate 'SWIFT Address' field is visible on LE360-LEdetails screen
    #When I navigate to 'LE360-LEdetails' task grid
    #And I 'SWIFT Address' field is visible on LE360-LEdetails screen
    # Validate 'SWIFT Address' field is visible, editable and non-mandatory on Update customer details screen for LEM workflow for client type 'FI'
    #Pre-requisite: Complete COB case with client type 'NBFI'
    #When I navigate to LE360 task screen
    #And I initiate "Maintenance Request" from action button
    #When I select 'LE details' from Area drop-down, 'KYC data and details' from LE details changes drop-down and submit the details
    #And I click on "CreateLEMSubmit" button
    # Verify LEM case has been triggered
    #Verify Non-material workflow is triggered
    #Then I see "CaptureProposedChangesGrid" task is generated
    #And I navigate to "CaptureProposedChangesGrid" task
    #When I complete "CaptureProposedChanges" screen
    #And I click on "SaveandCompleteCaptureProposedChanges" button
    #And I navigate to "UpdateCusotmerDetailsGrid" task
    #Validate 'SWIFT Address' field is visible, editable and non-mandatory on Update customer details screen
    #And I Validate 'SWIFT Address' field is visible, editable and non-mandatory on Update customer details screen
    #When I complete "UpdateCustomerDetails" screen
    #And I click on "SaveandCompleteUpdateCustomerDetails" button
    #Validate 'SWIFT Address' field is visible on LE360-LEdetails screen
    #When I navigate to 'LE360-LEdetails' task grid
    #And I 'SWIFT Address' field is visible on LE360-LEdetails screen
    # Validate 'SWIFT Address' field is visible, editable and non-mandatory on enrich KYC profile screen for Lite KYC workflow for client type 'FI'
    #Given I login to Fenergo Application with "RM:NBFI"
    #When I complete "NewRequest" screen select "Hedge Funds" for "Dropdown" field "Legal Entity Category" 
    #When I select "Yes" for "Dropdown" field "Does the CDD profile qualify for Lite KYC?"
    #And I click on "CreateEntity" button
    #And I navigate to "CaptureRequestDetailsGrid" task
    #And I complete "CaptureNewRequest" with Key "LiteKYC-NBFI" and below data
      #| Product | Relationship |
      #| C1      | C1           |
    #And I click on "Continue" button
    #When I complete "ReviewRequest" task
    #When I navigate to "ValidateKYCandRegulatoryGrid" task
    #When I complete "ValidateKYC" screen with key "FI"
    #And I click on "SaveandCompleteforValidateKYC" button
    #When I navigate to "EnrichKYCProfileGrid" task
    #Validate 'SWIFT Address' field is visible, editable and non-mandatory on enrich KYC profile
    #When I navigate to customer details sub-flow
    #Then I validate 'SWIFT Address' field is displaying as editable and non-mandatory
    #Validate 'SWIFT Address' field is visible on LE360-LEdetails screen
    #When I navigate to 'LE360-LEdetails' task grid
    #And I 'SWIFT Address' field is visible on LE360-LEdetails screen
