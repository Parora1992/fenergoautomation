#Test Case: TC_WAC_Corp_RR_02
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_WAC_Corp_RR_02

  Scenario: RR-Derive the overrall risk rating as Low (COB overall risk rating is Very High)
    #Refer to TC22 in the WAC Corp Data Sheet