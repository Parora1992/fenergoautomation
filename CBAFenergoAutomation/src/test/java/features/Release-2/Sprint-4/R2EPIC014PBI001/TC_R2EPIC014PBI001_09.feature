#Test Case: TC_R2EPIC014PBI001_09
#PBI: R2EPIC014PBI001
#User Story ID: NA
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2EPIC014PBI001_09

  Scenario: 
    ## Validate "Review start date" and "Compliance Review Date" populated correctly for closed case(Closed before implementation of this PBI) in the system for risk rating 'Low'.
    ## Validate "Review start date" and "Compliance Review Date" populated correctly for closed case(Closed before implementation of this PBI) in the system for risk rating 'Medium-Low'.
    ## Validate "Review start date" and "Compliance Review Date" populated correctly for closed case(Closed before implementation of this PBI) in the system for risk rating 'Medium'.
    ## Validate "Review start date" and "Compliance Review Date" populated correctly for closed case(Closed before implementation of this PBI) in the system for risk rating 'High'.
    ## Validate "Review start date" and "Compliance Review Date" populated correctly for closed case(Closed before implementation of this PBI) in the system for risk rating 'Very-High'.
    Given I login to Fenergo Application with "RM:IBG-DNE"
    #Validate Risk category as 'Low' and case has been closed before more than 90 days
    When I Search with 'CaseId' with CaseStatus as 'Closed'
    #Test-data: "Regular Review" workflow is triggered for this case
    When I navigate to "LE360-LE details" screen
    When I navigate to 'Cases' tab
    Then I see "regular Review" workflow is triggered for this case
    ##  Validate "Review start date" and "Compliance Review Date" populated correctly for closed case(Closed before implementation of this PBI) in the system for risk rating 'Medium-Low'
    Given I login to Fenergo Application with "RM:IBG-DNE"
    #Validate Risk category as 'Medium-Low' and case has been closed before more than 90 days
    When I Search with 'CaseId' with CaseStatus as 'Closed'
    #Test-data: "Regular Review" workflow is triggered for this case
    When I navigate to "LE360-LE details" screen
    When I navigate to 'Cases' tab
    Then I see "regular Review" workflow is triggered for this case
    ## Validate "Review start date" and "Compliance Review Date" populated correctly for closed case(Closed before implementation of this PBI) in the system for risk rating 'Medium'
    Given I login to Fenergo Application with "RM:IBG-DNE"
    #Validate Risk category as 'Medium' and case has been closed before more than 90 days
    When I Search with 'CaseId' with CaseStatus as 'Closed'
    #Test-data: "Regular Review" workflow is triggered for this case
    When I navigate to "LE360-LE details" screen
    When I navigate to 'Cases' tab
    Then I see "regular Review" workflow is triggered for this case
    ## Validate "Review start date" and "Compliance Review Date" populated correctly for closed case(Closed before implementation of this PBI) in the system for risk rating 'High'
    Given I login to Fenergo Application with "RM:IBG-DNE"
    #Validate Risk category as 'High' and case has been closed before more than 90 days
    When I Search with 'CaseId' with Case Status as 'Closed'
    #Test-data: "Regular Review" workflow is triggered for this case
    When I navigate to "LE360-LE details" screen
    When I navigate to 'Cases' tab
    Then I see "regular Review" workflow is triggered for this case
    ## Validate "Review start date" and "Compliance Review Date" populated correctly for closed case(Closed before implementation of this PBI) in the system for risk rating 'Very-High'
    Given I login to Fenergo Application with "RM:IBG-DNE"
    #Validate Risk category as 'Very-High' and case has been closed before more than 90 days
    When I Search with 'CaseId' with CaseStatus as 'Closed'
    #Test-data: "Regular Review" workflow is triggered for this case
    When I navigate to "LE360-LE details" screen
    When I navigate to 'Cases' tab
    Then I see "regular Review" workflow is triggered for this case
