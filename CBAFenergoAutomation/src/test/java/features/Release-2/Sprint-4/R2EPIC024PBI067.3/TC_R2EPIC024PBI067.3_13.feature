  #Test Case: TC_R2EPIC024PBI067.3_13
  #PBI: R2EPIC024PBI067.3
  #User Story ID: BL_067_004
  #Designed by: Sasmita Pradhan
  #Last Edited by:
  @COB
  Scenario: FI - Validate the field behaviours in "Source Of Funds And Wealth Details" Section in Enrich Client Information stage:
  #Validate fields "Value of Initial Deposit (AED),Source of Initial Deposit (AED),Details for Source of Initial Deposit,Country of Source of Initial Deposit, Projected Annual Business Turnover (AED) and Annual Business Turnover of Group (AED)" should not be visible on the UI
  #Field behavior in Source Of Funds And Wealth Details  section (1 new Data field should be added)
  #Validate field Details for Legal Entity Source of Income & Wealth  should be hidden from the UI
  #Validate field Legal Entity Source of Income & Wealth should be visible on screen under "Source Of Funds And Wealth Details " section
  #Validate the LoVs are displayed in the order mentioned in the PBI-LOV tab for the field "Legal Entity Source of Income & Wealth"
  #################################################################################
  #PreCondition:Create entity with client type as FI and confidential as FI with product type "Call Account or Savings "
  
    #######################################################################################
   
    Given I login to Fenergo Application with "RM:FI"            
    When I complete "NewRequest" screen with key "C1"
    And I complete "CaptureNewRequest" with Key "FI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryData" task
    When I complete "ValidateKYCandRegulatoryData" task
    When I navigate to "EnrichClientInformation" stage
    
    #Enrich KYC Profile screen
    #And I validate the following fields are not visible in "Business Details " section
   And I check that below fields are  not visible
      | FieldLabel                               |
      | Value of Initial Deposit (AED)           |
      | Source of Initial Deposit (AED)          |
      | Details for Source of Initial Deposit    |
      | Country of Source of Initial Deposit     |
      | Projected Annual Business Turnover (AED) |
      | Annual Business Turnover of Group (AED)  |
   
   
    #And I validate the following fields are not visible in "Source Of Funds And Wealth Details" section
   And I check that below fields are  not visible
      | FieldLabel                                          |
      | Legal Entity Source of Funds                        |
      | Details for Legal Entity Source of Income & Wealth  |
      | Details for Legal Entity Source of Funds            |
    #Condition: field "Legal Entity Source of Income & Wealth" should be visible if product is Product is "Call Account or Savings Account"   
   #And I validate the following fields are visible in "Source Of Funds And Wealth Details" section
   And I check that below fields are visible
      | FieldLabel                               |
      | Legal Entity Source of Income & Wealth   |
      
      
    #verify field behavior for the fields "Legal Entity Source of Income & Wealth"
   And I validate the following fields in "Source Of Funds And Wealth Details" section
    | Label                                    | Field Type            | Visible | Editable | Mandatory | Field Defaults To |
    | Legal Entity Source of Income & Wealth   | Multi-Select drop-down| Yes     | Yes      | No        |  Select...        |   
    #Validate the Value of Initial Deposit (AED) LoVs are displayed in the order mentioned in the PBI-LOV tab
    And I validate LOVs of "Value of Initial Deposit (AED)" field
    #Refer PBI for LOV list
    
    #Verify the field validation of 'Details for Legal Entity Source of Income & Wealth' when select value as "Other " from "Legal Entity Source of Income & Wealth" type field 
    And I select "Other" for "Legal Entity Source of Income & Wealth" field
    And I validate 'Details for Legal Entity Source of Income & Wealth' field is visible
    #verify field behavior for the conditional field "Details for Legal Entity Source of Income & Wealth"
   And I validate the following fields in "Source Of Funds And Wealth Details" section
    | Label                                              | Field Type    | Visible | Editable | Mandatory | Field Defaults To |
    | Details for Legal Entity Source of Income & Wealth | Alphanumeric  | Yes     | Yes      | Yes        |                   |
    
    #Verify field "Details for Legal Entity Source of Income & Wealth" should not be visible if user select "Others" as one of the value with additional one or more LOVs for field "Legal Entity Source of Income & Wealth"
    And I select "Other" with additional one more LOV for "Legal Entity Source of Income & Wealth" field
    And I validate 'Details for Legal Entity Source of Income & Wealth' field is not visible
    
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:FI"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:NBFI"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    And I complete "Waiting for UID from GLCMS" task from Actions button
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    
    When I navigate to LE Details screen 
    #LE Details screen
    
    #verify field behavior for the fields in "Source Of Funds And Wealth Details" section
    And I validate the following fields in "Source Of Funds And Wealth Details" section
   | Label                                              | Field Type     | Visible | Editable | Mandatory | Field Defaults To |
   | Legal Entity Source of Income & Wealth             |  drop-down     | Yes     | No       | Yes       |                   |
   | Details for Legal Entity Source of Income & Wealth | drop-down      | Yes     | No       | Yes       |                   |
   
    When I navigate to LE Verified Details screen 
    #LE Verified Details screen
    
   #verify field behavior for the fields in "Source Of Funds And Wealth Details" section
    And I validate the following fields in "Source Of Funds And Wealth Details" section
   | Label                                              | Field Type     | Visible | Editable | Mandatory | Field Defaults To |
   | Legal Entity Source of Income & Wealth             |  drop-down     | Yes     | No       | Yes       |                   |
   | Details for Legal Entity Source of Income & Wealth |  drop-down      | Yes     | No       | Yes       |                   |
      
   