#Test Case: TC_Backlog 71_02
#PBI: Backlog 71
#User Story ID:
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_Backlog 71_02

  Scenario: Validate 'fenergo' logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen across all the tasks of Lite KYC workflow
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Given I login to Fenergo Application with "RM:FI"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I complete "NewRequest" screen with key "LiteKYC-FI"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    And I complete "CaptureNewRequest" with Key "LiteKYC-FI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "LiteKYC-FI"
    And I click on "SaveandCompleteforValidateKYC" button
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "EnrichKYCProfileGrid" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I complete "EnrichKYC" screen with key "LiteKYC-FI"
    And I click on "SaveandCompleteforEnrichKYC" button
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I complete "Documents" in "Edit Verification" screen
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I complete "CompleteID&V" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessment" screen with key "LiteKYC"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Then I login to Fenergo Application with "RM:FI"
    When I search for the "CaseId"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Then I login to Fenergo Application with "BUH:FI"
    When I search for the "CaseId"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Then I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
