#Test Case: TC_R2EPIC014PBI003_06
#PBI: R2EPIC014PBI003
#User Story ID:
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R2EPIC014PBI003_06 

@Automation 
Scenario:
FI:Validate the field behaviours in Business Details section of Review/Edit Client Data Screen in RR workflow 
#Precondition: COB case with Client type as FI and COI as UAE to be created by filling all the mandatory and non-mandatory fields and Case status should be Closed
	Given I login to Fenergo Application with "RM:FI" 
	When I complete "NewRequest" screen with key "FI" 
	And I complete "CaptureNewRequest" with Key "FI" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	Given I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "FI" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I complete "AddAddressFAB" task 
	When I complete "EnrichKYC" screen with key "FI" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task 
	When I complete "RiskAssessment" task 
	Then I login to Fenergo Application with "RM:FI" 
	When I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "BUH:FI" 
	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	And I complete "Waiting for UID from GLCMS" task from Actions button 
	When I navigate to "CaptureFabReferencesGrid" task 
	When I complete "CaptureFABReferences" task 
	And I assert that the CaseStatus is "Closed" 
#	# =Initiate Regular Review
	And I initiate "Regular Review" from action button 
	# =Then I navigate to "CloseAssociatedCasesGrid" task 
	When I complete "CloseAssociatedCase" task 
	Then I store the "CaseId" from LE360 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "FI" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "Review/EditClientDataTask" task 
	And I validate the following fields in "Business Details" Sub Flow 
		| Label                       | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo |
		| Nature of Business Activity | TextBox   | true    | false    | true      | NA         |
		| Annual Business Turnover    | NA        | false   | NA       | NA        | NA         |
	And I validate the following fields in "Business Details" Sub Flow 
		| Label                                                    | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo |
		| Primary Business Activity                                | TextBox   | true    | false    | false     | NA         |
		| Anticipated Transactions Turnover (Annual in AED)        | NA        | false   | NA       | NA        | NA         |
		| Value of Initial Deposit (AED)                           | NA        | false   | NA       | NA        | NA         |
		| Source of Initial Deposit & Country of Source            | NA        | false   | NA       | NA        | NA         |
		| Value of Capital or Initial Investment in Business (AED) | NA        | false   | NA       | NA        | NA         |
		| Projected Annual Business Turnover (AED)                 | NA        | false   | NA       | NA        | NA         |
		| Annual Business Turnover of Group (AED)                  | NA        | false   | NA       | NA        | NA         |
		| Active Presence in Sanctioned Countries/Territories      | Dropdown  | true    | false    | NA        | NA         |
		| If Yes, Specify the Sanctioned Countries/Territories     | NA        | false   | NA       | NA        | NA         |
		| Offshore Banking License                                 | Dropdown  | true    | false    | NA        | NA         |
	When I select "No" for "Dropdown" field "Active Presence in Sanctioned Countries/Territories" 
	And I validate the following fields in "Business Details" Sub Flow 
		| Label                                                | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo |
		| If Yes, Specify the Sanctioned Countries/Territories | NA        | false   | NA       | NA        | NA         |
	When I select "Yes" for "Dropdown" field "Active Presence in Sanctioned Countries/Territories" 
	And I validate the following fields in "Business Details" Sub Flow 
		| Label                                                | FieldType           | Visible | ReadOnly | Mandatory | DefaultsTo |
		| If Yes, Specify the Sanctioned Countries/Territories | MultiSelectDropdown | true    | false    | NA        | NA         |
	And I check that below data is not visible 
		| FieldLabel                       |
		| Payment Countries                |
		| Anticipated Transactions Profile |
		| Payment Countries                |
		| Anticipated Transactions Profile |
	And I fill "Alphanumeric" data of length "256" in "Name of Registration Body" field 
	And I validate the error messgage for "Name of Registration Body" as "You've reached the maximum length. Name of Registration Body accepts 255 characters." 
	And I fill "Alphanumeric" data of length "254" in "Name of Registration Body" field 
	And I fill "Alphanumeric" data of length "255" in "Name of Registration Body" field 
	And I fill "Alphanumeric" data of length "257" in "Primary Business Activity" field 
	And I validate the error messgage for "Primary Business Activity" as "You've reached the maximum length. Primary Business Activity accepts 256 characters." 
	And I fill "Alphanumeric" data of length "255" in "Primary Business Activity" field 
	And I fill "Alphanumeric" data of length "256" in "Primary Business Activity" field 
	When I select "Yes" for "Dropdown" field "Active Presence in Sanctioned Countries/Territories" 
	And I verify "If Yes, Specify the Sanctioned Countries/Territories" drop-down values 
	And I verify "Active Presence in Sanctioned Countries/Territories" drop-down values 
	And I verify "Offshore Banking License" drop-down values 
	
	
	
	
	
	
	
	
	
	#-> Written for Manual Testing
	#Given I login to Fenergo Application with "KYCMaker_FIG"
	#When I search for the "CaseId"
	#Initiate Regular Review
	#When I select "RegularReview" from Actions menu
	#When I navigate to "CloseAssociatedCases" task
	#And I click on "SaveandComplete" button
	#When I navigate to "ValidateKYCandRegulatoryGrid" task
	#When I complete "ValidateKYC" screen with key "C1"
	#And I click on "SaveandCompleteforValidateKYC" button
	#When I navigate to "ReviewEditClientData" task
	#Validate in Business details section the below fields are available (Label change) and values are autopopulated from the COB case
	#Validate the field type, visibility, editable and mandatory and field values are defautled from COB
	#And I validate the following fields in "Customer Details" Sub Flow
	#| FieldLabel                    | Field Type   | Visible | Editable | Mandatory | Field Defaults To       |
	#| Nature of Activity / Business | Alphanumeric | true    | false    | true      | Auto populated from COB |
	#| Annual Business Turnover      |              | false   |          |           |                         |
	#Validate the below new fields are available (new fields & Existing fields) in Business details section and values are autopopulated from COB case
	#Validate the field type, visibility, editable and mandatory and field values are defautled from COB
	#And I validate the following fields in "Business Details" Sub Flow
	#| FieldLabel                                               | Field Type   | Visible | Editable | Mandatory | Field Defaults To       |
	#| Primary Business Activity                                | Alphanumeric | true    | true     | false     | Auto populated from COB |
	#| Anticipated Transactions Turnover (Annual in AED)        |              | false   |          |           |                         |
	#| Value of Initial Deposit (AED)                           |              | false   |          |           |                         |
	#| Source of Initial Deposit & Country of Source            |              | false   |          |           |                         |
	#| Value of Capital or Initial Investment in Business (AED) |              | false   |          |           |                         |
	#| Projected Annual Business Turnover (AED)                 |              | false   |          |           |                         |
	#| Annual Business Turnover of Group (AED)                  |              | false   |          |           |                         |
	#| Active Presence in Sanctioned Countries/Territories      | Drop-down    | true    | true     | false     | Auto populated from COB |
	#| If Yes, Specify the Sanctioned Countries/Territories     |              | false   |          |           |                         |
	#| Offshore Banking License                                 | Drop-down    | true    | true     | false     | Auto populated from COB |
	#Verify the field 'If Yes, Specify the Sanctioned Countries/Territories' is visible when value 'Yes' is selected for the field 'Active Presence in Sanctioned Countries/Territories'
	#And I validate the following fields in "Business Details" Sub Flow
	#| FieldLabel                                           | Field Type | Visible | Editable | Mandatory | Field Defaults To       |
	#| If Yes, Specify the Sanctioned Countries/Territories | Drop-down  | true    | true     | false     | Auto populated from COB |
	##
	#Verify the field 'If Yes, Specify the Sanctioned Countries/Territories' is hidden when value 'NO' is selected for the field 'Active Presence in Sanctioned Countries/Territories'
	#And I validate the following fields in "Business Details" Sub Flow
	#| FieldLabel                                           | Field Type | Visible | Editable | Mandatory | Field Defaults To       |
	#| If Yes, Specify the Sanctioned Countries/Territories |            | false   |          |           | Auto populated from COB |
	#Validate the below fields are hidden in Business details section
	#And I validate the following fields in "Business Details" Sub Flow
	#| Label                            | Visible |
	#| Payment Countries                | false   |
	#| Anticipated Transactions Profile | false   |
	##
	#Field Validation for 'Nature of Activity / Business' field
	#Verify 'Name of Registration Body' field NOT accepts more than 1000 Alphanumeric characters. Test data : 1001 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data8"
	#Verify Legal Entity Name field accepts less than 1000 Alphanumeric characters. Test data : 999 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data9"
	#Verify Legal Entity Name field accepts 1000 Alphanumeric characters. Test data : 1000 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data10"
	##
	#Field Validation for 'Primary Business Activity
	#Verify 'Primary Business Activity' field NOT accepts more than 256 Alphanumeric characters. Test data : 257 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data8"
	#Verify 'Primary Business Activity' field accepts less than 256 Alphanumeric characters. Test data : 255 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data9"
	#Verify 'Primary Business Activity' field accepts 256 Alphanumeric characters. Test data : 256 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data10"
	##
	#LOV validation - If Yes, Specify the Sanctioned Countries/Territories. (Refer to Countries in the PBI-LOV tab)
	#And I validate the LOV of "CountryofIncorporation/Establishment" with key "countrieslov"
	##
	#LOV validation - Active Presence in Sanctioned Countries/Territories.
	#Then I validate the specific LOVs for "ActivePresenceinSanctionedCountries/Territories"
	#| Lovs |
	#| Yes  |
	#| NO   |
	#LOV validation - Offshore Banking License.
	#Then I validate the specific LOVs for "OffshoreBankingLicense"
	#| Lovs |
	#| Yes  |
	#| NO   |
