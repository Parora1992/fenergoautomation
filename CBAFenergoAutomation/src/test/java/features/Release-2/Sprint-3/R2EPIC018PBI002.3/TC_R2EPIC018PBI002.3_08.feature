Feature: LEM - KYC Data and Customer Details - Data Attributes Changes
#Test Case: TC_R2EPIC018PBI002.3_08
#PBI: R2EPIC018PBI002.3v0.2
#User Story ID: NA
#Designed by: Sasmita Pradhan
#Last Edited by: Sasmita Pradhan
@TobeAutomated @LEM
Scenario: Corporate - Validate the behavior of "Capture Proposed Changes" task when area is 'LE Details' and Le Details Changes is 'KYC Data and Customer details' during LEM :
#Area for Change section - No change (OOTB feature)
#'Business Markets' subflow should be hidden
#Comments section - No change (OOTB feature) 
#Field behavior in Customer Details section (11 fields should be hidden, 8 new fields should be added, 9 fields should be modified) 
#Field behavior in KYC Conditions section (7 fields should be hidden, 5 new fields should be added, 12 fields should be modified)
#Field behavior in  Regulatory Data section (1field should be modified)

#Precondition: Create COB with Client Type = FI and country of incorporation = AE-UNITED ARAB EMIRATES and following data in "Validate KYC and Regulatory Data" task and case status should be closed
 ##Is this entity publicly listed? - No
##Is this a Prohibited client (as per FAB's AML/CTF/Sanctions Policy)?-Yes



Given I login to Fenergo Application with "KYCMaker: Corporate"
When I search for the "CaseId"
#LEM flow starts	
#Initiate "Maintenance Request" 
And I initiate "Maintenance Request" from action button
When I select "LEDetails" for field "Area"
When I select "KYC Data and Customer details" for field "LE Details Changes"  
And I fill " Test" field " Reason" 
And I submit the "Maintenance Request"
And I assert that "Capture Proposed Changes" task is triggered
And I navigate to "Capture Proposed Changes" task
#Validate the behavior of "Capture Proposed Changes" task when area is LE Details and Led Details Changes is 'KYC Data and Customer details'
#Area for Change section - No change (OOTB feature) 
And I validate the following fields in "Area for Change" section 
      | Label  | FieldType    | Visible | ReadOnly | DefaultsTo                    |
      | Area   | Dropdown     | true    | true     |KYC Data and Customer details  |
      | Reason | Alphanumeric | true    | true     | Test                          |

#'Business Markets' subflow should be hidden
And I assert "Business Markets" subflow is not visible
##Field behavior in Customer Details section (11 fields should be hidden, 8 new fields should be added, 9 fields should be modified)
And I validate the following fields in "Customer Details " section #Only the following fields should be present in the mentioned order
| FieldLabel   								                  | Field Type       | Visible      | Editable     | Mandatory   | DefaultsTo               |
| ID														  | Alphanumeric     |Yes           | No           | No          | Auto populated           |           
| FAB Segment                                                 |Drop-down         |Yes           | Yes          | Yes         |                          |
| Client Type                                                 |Drop-down         |Yes           | No           | No          |                          | 
| Legal Entity Name                                           |Alphanumeric      |Yes           | Yes          | Yes         |  Auto-populated from COB |                    
| Legal Entity Type                                           |Drop-down         |Yes           | No           | No          |                          |
| Entity Type                                                 |Drop-down         |Yes           | No           | No          |                          |
| Country of Incorporation / Establishment                    |Drop-down         |Conditional   | Yes          | No          |                          | 
| Country of Domicile/ Physical Presence                      |Drop-down         |Yes           | Yes          | No          |                          |
| Date of Incorporation                                       |Date              |Yes           | Yes          | No          |                          |
| Purpose of Account/Relationship                             |Alphanumeric      |Yes           | Yes          | No          |                          |
| Residential Status                                          |Drop-down         |Yes           | No           | No          |  Resident                         |                      
| Relationship with bank                                      |Drop-down         |Yes           | Yes          |  Yes        |                          |
| CIF Creation Required?                                      |Check box         |Yes           | Yes          |  Yes        |  Yes                     |
| Emirate                                                     |Drop-down         |Yes           | Conditiona   |  Conditional|  
                        
And I validate No special characters allowed for field Legal Entity Type
#Only A-Z, a-z and 0-9  and(period/full stop/space) allowed for field Legal Entity Type
And I validate date cannot be in the future for field Date of Incorporation
#LOV validation - FAB Segment. (Refer PBI-LOV tab)
And I validate the LOV for the field "FAB Segment"


#Validate the below fields are not visible under "Customer Details" section on "Capture Proposed Changes" screen
And I validate below fields are not visible under "Customer Details" section on "Capture Proposed Changes" screen
| FieldLabel                                                      |
| Registration Number                                             |
| Principal Place of Business                                     | 
| Group Name                                                      |
| Is this entity publicly listed?                                 |
| Name of Stock Exchange                                          |
| Stock Exchange Domiciled in FAB's Recognised List of Countries? |
| Country of Stock Exchange                                       |
| Is this entity regulated?                                       |
| Regulated By                                                    |
| Regulatory ID                                                   |
| SWIFT BIC                                                       |


##Field behavior in KYC Conditions section (7 fields should be hidden, 5 new fields should be added, 12 fields should be modified) 
And I validate the following fields in "KYC Conditions " section #Only the following fields should be present in the mentioned order
| FieldLabel                                                                      | Field Type               | Visible | Editable    | Mandatory   | DefaultsTo        |
| Legal Entity Type                                                               | Drop-down                | Yes     | No          | No          | Auto populated    |
| Country of Incorporation / Establishment                                        | Drop-down                | Yes     | Yes         | No          | Auto populated    |
| Is this entity publicly listed?                                                 | Drop-down                | Yes     | yes         | Yes         |  No                 |
| Is this parent entity publicly listed?                                          | Drop-down                | Yes     | Conditional | Conditional |  Yes                 |
| Name of Stock Exchange                                                          | Multiselected Drop-down  | Yes     | Conditional | Conditional |  Yes                 |
| Stock Exchange Domicile Country                                                 | Multiselected Drop-down  | Yes     | Conditional | Conditional |                   |
| Is this FAB's Recognized Stock Exchange?                                        | Drop-down                | Yes     | Conditional | Conditional |                   |
| Is this a Prohibited client (as per FAB’s AML/CTF/Sanctions Policy)?            | Drop-down                | Yes     | Yes         | Yes         |    Yes               |
| Specify the prohibited Category                                                 | Alphanumeric             | Yes     | Conditional | Conditional |                   |
| Has the exceptional approvals been obtained to on-board/retain the client?      | Drop-down                | Yes     | Conditional | Conditional |                   |                  |                   |
| Is this entity regulated?                                                       | Drop-down                | Yes     | Conditional | NO          |  Yes|
| Name of Regulatory Body                                                         | Drop-down                | Yes     | Yes         | Conditional | Auto populated    |
| Regulatory ID                                                                   | Alphanumeric             | Yes     | Conditional | NO          |                   |
| Is there an AML program in place?                                               | Drop-down                | Yes     | Conditional | NO          |                   |
| Is the entity a wholly-owned subsidiary of a parent?                            | Drop-down                | Yes     | Conditional | Conditional |                   |
| Types of Shares (Bearer/Registered)                                             | Drop-down                | Yes     | yes         | Yes         |                   |
| Is the Entity operating with Flexi Desk?'                                       | Drop-down                | Yes     | Conditional | Conditional |                   |
| Is UAE Licensed                                                                 | Auto                     | Yes     | yes         | No          |  Yes                 |

#Validate the below fields are not visible under "KYC Conditions" section on "Capture Proposed Changes" screen
And I Validate the below fields are not visible under "KYC Conditions" section on "Capture Proposed Changes" screen
| FieldLabel                                         |
| Legal Entity Category                              |
| Country of Domicile                                |
| Parent Company: Country of Incorporation           |
| Is the parent entity regulated?                    |
| Parent Regulated By                                |                                                
| Parent's AML Guidelines Reviewed and Approved      |
| Name of Exchange(s) the Parent Entity is Listed On |

##Field behavior in Regulatory Data section 
And I validate the following fields in " Regulatory Data " section #Only the following fields should be present in the mentioned order
| FieldLabel                                           | Field Type            | Visible | Editable    | Mandatory   | DefaultsTo   |
| Countries of Business Operations/Economic Activity   | MultiSelect Drop-down | Yes     | Yes         | No          |              |
| Is the entity an Intragroup entity?                  | Drop-down             | Yes     | No          | No          |              |
| Dodd-Frank US Person Type                            | Drop-down             | Yes     | No          | No          |              |
| Direct Electronic Access Client?                     | Drop-down             | Yes     | No          | No          |              |


                    
