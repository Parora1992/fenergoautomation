Feature: LEM - KYC Data and Customer Details - Data Attributes Changes
#Test Case: TC_R2EPIC018PBI002.3_05
#PBI: R2EPIC018PBI002.3v0.2
#User Story ID: NA
#Designed by: Sasmita Pradhan
#Last Edited by: Sasmita Pradhan
@TobeAutomated @LEM
Scenario: Corporate - Validate the behavior of "Update Customer Details " task when area is 'LE Details' and Le Details Changes is 'KYC Data and Customer details' during LEM :
#Comments section - No change (OOTB feature) 
#Addresses section -  No change(OOTB feature) 
#'Business Markets' subflow should be hidden
#'Financial Institution Due Diligence'subflow should be hidden
#'Anticipated Transaction Volumes (Annual in AED)' section should be hidden
#Field behavior in Customer Details section (8 fields should be hidden, 13 new fields should be added, 5 fields should be modified) 
#Field behavior in Business Details section (2 fields should be hidden, 9 new fields should be added, 2 fields should be modified)
#Field behavior in Source Of Funds And Wealth Details section (2 field should be modified)
#Field behavior in Industry Codes Details section (7 fields should be hidden, 1 new field should be added, 2 fields should be modified)
#"Primary Industry of Operation" ,"Secondary Industry of Operation ","Primary Industry of Operation Islamic" and "Primary Industry of Operation UAE "field should be greyed out
#Field behavior in Internal Booking Details section (4 fields should be hidden, 3 new fields should be added, 1 field should be modified)
#New panel Anticipated Transactional Activity(Per Month) should be added on Update Customer Details Screen
###Field behavior in Anticipated Transactional Activity (Per Month) section 



#Precondition: Create COB with Client Type = Corporate and country of incorporation = AE-UNITED ARAB EMIRATES and following data in "Validate KYC and Regulatory Data" task and case status should be closed
 ##Is this entity publicly listed? - No
##Is this a Prohibited client (as per FAB's AML/CTF/Sanctions Policy)?-Yes



Given I login to Fenergo Application with "KYCMaker: Corporate"
When I search for the "CaseId"
#LEM flow starts	
#Initiate "Maintenance Request" 
And I initiate "Maintenance Request" from action button
When I select "LEDetails" for field "Area"
When I select "KYC Data and Customer details" for field "LE Details Changes"  
And I fill " Test" field " Reason" 
And I submit the "Maintenance Request"
And I assert that "Capture Proposed Changes" task is triggered
And I navigate to "Capture Proposed Changes" task
And I complete the "Capture Proposed Changes" task
And I assert that "Update Customer Details" task is triggered
When I navigate to "Update Customer Details" task 

#Validate the behavior of "Update Customer Details " task when area is LE Details and Led Details Changes is 'KYC Data and Customer details'

And I assert "Business Markets",'Financial Institution Due Diligence'subflow and 'Anticipated Transaction Volumes (Annual in AED)' section is not visible on "Update Customer Details" screen
And I assert "Anticipated Transactional Activity(Per Month) panel is visible on " Update Customer Details" screen
##Field behavior in Customer Details section (8 fields should be hidden, 13 new fields should be added, 5 fields should be modified) 
And I validate the following fields in "Customer Details " section #Only the following fields should be present in the mentioned order
| FieldLabel   								                  | Field Type       | Visible      | Editable     | Mandatory   | DefaultsTo               |
| ID														  | Alphanumeric     |Yes           | No           | No          | Auto-populated           |           
| Client Type                                                 | Drop-down        |Yes           | No           | No          | N/A                      |
| Legal Entity Name                                           | Alphanumeric     |Yes           | No           | No          | N/A                      |
| Legal Entity Type                                           | Drop-down        |Yes           | No           | No          | N/A                      |
| Entity Type                                                 | Drop-down        |Yes           | No           | No          | Auto-populated           |
| LEI                                                         | Alphanumeric     |Yes           | Yes          | No          | Auto-populated           |
| Country of Incorporation / Establishment                    | Drop-down        |Yes           | Yes          | Yes         |                          |
| Country of Domicile/ Physical Presence                      | Drop-down        |Yes           | Yes          | Yes         |                          |
| Date of Incorporation / Establishment                       | Date             |Yes           | Yes          | Yes         |                          |
| GLEIF Legal Name                                            | Alphanumeric     |Yes           | Yes          | No          | Auto-populated           |
| GLEIF Legal Jurisdiction                                    | Alphanumeric     |Yes           | Yes          | No          | Auto-populated           |
| GLEIF Legal Form                                            | Alphanumeric     |Yes           | Yes          | No          | Auto-populated           |
| GLEIF Entity Status                                         | Alphanumeric     |Yes           | Yes          | No          | Auto-populated           |
| Registration Number                                         | Alphanumeric     |Yes           | Yes          | No          | Auto-populated           |
| Name of Registration Body                                   | Alphanumeric     |Yes           | Yes          | Yes         | Auto-populated           |
| Customer Relationship Status                                | Drop-down        |Yes           | No           | No          | Autopopulated from GLCMS |
| Length of Relationship                                      | Drop-down        |Yes           | Yes          | Yes         |                          |
| Does the entity have a previous name(s)?                    | Drop-down        |Yes           | Yes          | Yes         | Yes                      |
| Previous Name(s)                                            | Alphanumeric     |Yes           | Yes          | Yes         |                          |
| Trading/Operation Name                                      | Alphanumeric     |Yes           | Yes          | No          |                          |
| Legal Entity Name (Parent)                                  | Alphanumeric     |Yes           | Yes          | No          |                          |
| Website Address                                             | Alphanumeric     |Yes           | Yes          | No          |                          |
| Real Name                                                   | Alphanumeric     |Yes           | Yes          | No          |                          |
| Original Name                                               | Alphanumeric     |Yes           | Yes          | No          |                          |
| Entity Level                                                | Drop-down        |Conditional   | Conditional  | Conditional |                          |
| Legal Counter party type                                    | Drop-down        |Yes           | Yes          | Yes         | N/A                      |
| Legal Constitution Type                                     | Drop-down        |Yes           | Yes          | Yes         |                          |
| Emirate                                                     | Drop-down        |Yes           | No           | Yes         |                          |
| Group Name                                                  | Alphanumeric     |Yes           | Yes          | No          |  
And I validate No special characters / No lowercase characters allowed for field "Trading/Operation Name"and Legal Entity Name (Parent) type
And I validate date cannot be in the future for field Date of Incorporation                        |
#Validate the below fields are not visible under "Customer Details" section on "Update Customer Details" screen
And I validate below fields are not visible under "Customer Details" section on "Update Customer Details" screen
| FieldLabel                               |
| FAB Segment                              |
| Legal Entity Category                    | 
| Anticipated Activity of Account          |
| Primary Business Location                |
| Residential Status                       |
| Channel & Interface                      |
| Can the corporation issue bearer shares? |
| Legal Status                             |
                                                     
##Field behavior in Business Details section (2 fields should be hidden, 9 new fields should be added, 2 fields should be modified)
And I validate the following fields in "Business Details" section #Only the following fields should be present in the mentioned order
| FieldLabel                                                | Field Type               | Visible | Editable    | Mandatory   | DefaultsTo        |
| Nature of Business Activity                               | Alphanumeric             | Yes     | Yes         | Yes         | N/A               |
| Primary Business Activity                                 | Alphanumeric             | Yes     | Yes         | No          | N/A               |
| Anticipated Transactions Turnover (Annual in AED)         | Drop-down                | Yes     | yes         | Yes         |                   |
| Value of Initial Deposit (AED)                            | Numeric                  | Yes     | Yes         | Yes         |                   |
| Source of Initial Deposit & Country of Source             | Alphanumeric             | Yes     | Yes         | Yes         |                   |
| Value of Capital or Initial Investment in Business (AED)  | Numeric                  | Yes     | Yes         | No          |                   |
| Annual Business Turnover (AED)                            | Numeric                  | Yes     | Yes         | No          |                   |
| Projected Annual Business Turnover (AED)                  | Numeric                  | Yes     | Yes         | No          |                   |
| Annual Business Turnover of Group (AED)                   | Numeric                  | Yes     | Yes         | No          |                   |

#Validate the below fields are not visible under "Business Details" section
And I Validate the below fields are not visible under "Business Details" section 
| FieldLabel                        |
| Payment Countries                 |
| Anticipated Transactions Profile  |


##Field behavior in Source Of Funds And Wealth Details section 
And I validate the following fields in " Source Of Funds And Wealth Details " section #Only the following fields should be present in the mentioned order
| FieldLabel                                                             | Field Type            | Visible | Editable    | Mandatory   | DefaultsTo   |
| Legal Entity Source of Wealth  Legal Entity Source of Income & Wealth  | Drop-down String      | Yes     | Yes         | No          |              |
| Legal Entity Source of Funds                                           | Drop-down String      | Yes     | No          | No          |              |

##Field behavior in Industry Codes Details section (7 fields should be hidden, 9 new fields should be added, 2 fields should be modified)
And I validate the following fields in "Industry Codes Details" section #Only the following fields should be present in the mentioned order
| FieldLabel                            | Field Type               | Visible | Editable    | Mandatory   | DefaultsTo        |
| Primary Industry of Operation         | Drop-down                | Yes     | Yes         | Yes         |                   |
| Secondary Industry of Operation       | Drop-down                | Yes     | Yes         | No          | N/A               |
| Primary Industry of Operation Islamic | Conditional              | Yes     | yes         | Yes         |                   |
| Primary Industry of Operation UAE)    | Drop-down                | Yes     | Yes         | Yes         |                   |
##Validate if above mentioned fields should be greyed out
And I assert below fields are greyed out
| FieldLabel                            |
| Primary Industry of Operation         | 
| Secondary Industry of Operation       | 
| Primary Industry of Operation Islamic | 
| Primary Industry of Operation UAE)    |


#Validate the below fields are not visible under "Industry Codes Details" section
And I Validate the below fields are not visible under "Industry Codes Details" section 
| FieldLabel          |
| NAIC                |
| ISIN                |
| Secondary ISIC      |
| NACE 2 Code         |
| Stock Exchange Code |
| Central Index Key   |
| SWIFT BIC           |      

             
##Field behavior in Internal Booking Details section (4 fields should be hidden, 3 new fields should be added, 1 field should be modified)
And I validate the following fields in "Internal Booking Details " section #Only the following fields should be present in the mentioned order
| FieldLabel                                | Field Type               | Visible | Editable    | Mandatory   | DefaultsTo        |
| Request Type                              | Drop-down                | Yes     | No          | No          |  Auto-populated   |
| SRequest Origin                           | Date                     | Yes     | No          | No          |  N/A              |
| Client Reference                          | Alphanumeric             | Yes     | yes         | No          |                   |
| Booking Country                           | Drop-down                | Yes     | Yes         | Yes         |                   |
| Consented to Data Sharing Jurisdictions   | MultiDrop-down           | Yes     | Yes         | No          |                   |
| Confidential                              | Drop-down                | Yes     | Yes         | Yes         |  N/A              |
| Jurisdiction                              | N/A                      | Yes     | No          | No          |                   |
| Sector Description                        | Drop-down                | Yes     | Yes         | No          |                   |
| UID Originating Branch                    | Drop-down                | Yes     | Yes         | Yes         |                   |
| Propagate To Target Systems               | Alphanumeric             | Yes     | No          | Yes         |                   |
#Validate the below fields are not visible under "Internal Booking Details" section
And I Validate the below fields are not visible under "Internal Booking Details" section 
| FieldLabel         |
| Priority           |
| IFrom Office Area  |
| On Behalf Of       |
| Internal Desk      |

When I navigate to Anticipated Transactional Activity (Per Month) panel
##Field behavior in Anticipated Transactional Activity (Per Month) panel  
And I validate the following fields in "Anticipated Transactional Activity (Per Month) " section #Only the following fields should be present in the mentioned order
| FieldLabel                                | Field Type               | Visible | Editable    | Mandatory   | DefaultsTo        |
| ID                                        | Alphanumeric             | Yes     | No          | No          |  Auto-populated   |
| Value of Projected Transactions (AED)     | Numeric                  | Yes     | Yes         | No          |                   |
| Number of Transactions                    | Numeric                  | Yes     | Yes         | No          |                   |       
| Type/Mode of Transactions                 | Alphanumeric             | Yes     | No          | No          |                   |
| Transaction Type                          | Drop-down                | Yes     | Yes          | No          |                   |
| Currencies Involved                       | MultiDrop-down           | Yes     | Yes          | No          |                   |
| Countries Involved                        | MultiDrop-down           | Yes     | Yes          | No          |                   |



