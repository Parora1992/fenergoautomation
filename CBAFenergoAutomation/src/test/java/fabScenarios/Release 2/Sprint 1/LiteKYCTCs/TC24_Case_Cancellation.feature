#Test Case: TC24_Case_Cancellation
#Designed by: Vibhav Kumar
#Last Edited by: Vibhav Kumar
Feature: TC24_Case_Cancellation

  @Automation
  Scenario: Verify that KYC Maker user is able to cancel the case
    
    Given I login to Fenergo Application with "RM:FI"
    When I navigate to "LegalEntityCategoryWithoutSubmit" button with ClientType as "Financial Institution (FI)"
    When I select "Hedge Funds" for "Dropdown" field "Legal Entity Category"
    When I select "Client/Counterparty" for "Dropdown" field "Legal Entity Role"
    When I select "Bank - France Branch" for "Dropdown" field "Entity of Onboarding"
    When I select "Hedge Funds" for "Dropdown" field "Legal Entity Category"
    When I select "Yes" for "Dropdown" field "Does the CDD profile qualify for Lite KYC?"
    And I click on "CreateEntity" button
    And I complete "CaptureNewRequest" with Key "LiteKYC-FI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    Then I cancel the case
    #And I assert that the CaseStatus is "Closed"
    
    
    